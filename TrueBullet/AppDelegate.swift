//
//  AppDelegate.swift
//  TrueBullet
//
//  Created by Milica Cokic on 4/22/18.
//  Copyright © 2018 Milica Cokic. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    static var window: UIWindow?
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        AppDelegate.window = UIWindow(frame: UIScreen.main.bounds)
        
//        let defaults = UserDefaults.standard
//        if defaults.object(forKey: "isFirstTime") == nil {
//            defaults.set("No", forKey:"isFirstTime")
//            defaults.synchronize()
//            let storyboard = UIStoryboard(name: "Main", bundle: nil) //Write your storyboard name
//            guard let termsVC = storyboard.instantiateViewController(withIdentifier: "TermsViewController") as? TermsViewController else {
//                fatalError("could not find TermsViewController")
//            }
//            AppDelegate.window?.rootViewController = termsVC
//            AppDelegate.window?.makeKeyAndVisible()
//        }
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let tabBarVC = storyboard.instantiateInitialViewController()
        
        AppDelegate.window?.rootViewController = tabBarVC
        
        if let tabVC = tabBarVC as? UITabBarController {
             if #available(iOS 13.0, *) {
                tabVC.tabBar.barTintColor = UIColor.systemBackground
            } else {
                tabVC.tabBar.barTintColor = UIColor.white
            }
        }
       
        AppDelegate.window?.makeKeyAndVisible()
        
        return true
    }
}

