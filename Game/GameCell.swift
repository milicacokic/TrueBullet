//
//  GameCell.swift
//  TrueBullet
//
//  Created by Milica Cokic on 5/1/18.
//  Copyright © 2018 Milica Cokic. All rights reserved.
//

import UIKit

final class GameCell: UITableViewCell, ReusableView {
    
    @IBOutlet private weak var labelName: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        cleanup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        addSeparatorLineToTop()
    }
    
    
    func configure(with cc: GameSpecific) {
        labelName.text = cc.name
    }
    
}


private extension GameCell {
    
    func cleanup() {
        labelName.text = nil
        
    }
    
}
