//
//  GameCategoryCell.swift
//  TrueBullet
//
//  Created by Milica on 11/27/18.
//  Copyright © 2018 Milica Cokic. All rights reserved.
//

import UIKit

final class GameCategoryCell: UICollectionViewCell, ReusableView {
    
    //@IBOutlet private weak var label: UILabel!
    @IBOutlet private weak var cellImage: UIImageView!
    @IBOutlet private weak var cellText: UILabel!
    
    func populate(with image: UIImage, and text: String) {
        cellImage.image = image
        cellText.text = text
    }
}
