//
//  GameViewController.swift
//  TrueBullet
//
//  Created by Milica on 11/27/18.
//  Copyright © 2018 Milica Cokic. All rights reserved.
//

import UIKit

final class GameViewController: UIViewController {
    
    @IBOutlet private weak var collectionView: UICollectionView!
    @IBOutlet private weak var tableView: UITableView!
    
    private var myGameStorage = GameStorage.shared
    
    lazy var searchBar = UISearchBar(frame: CGRect.zero)
    private var searchActive: Bool = false
    
    private var headerLabel: String = "Small"
    private var cellTitles: [String] = ["Small", "Very Light", "Light", "Medium", "Large", "Very Large", "Heavy", "Super Heavy", "Dangerous"]
    
    private var filteredByCategory: [GameSpecific] = []
    private var filteredGames: [GameSpecific] = []
    
    private var small: [GameSpecific] = []
    private var extraLight: [GameSpecific] = []
    private var light: [GameSpecific] = []
    private var medium: [GameSpecific] = []
    private var large: [GameSpecific] = []
    private var veryLarge: [GameSpecific] = []
    private var heavy: [GameSpecific] = []
    private var superHeavy: [GameSpecific] = []
    private var dangerous: [GameSpecific] = []
    
    private var index: Int = 0
    private var isArrayArrived = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false
    
        setupKeyboardNotificationHandlers()
        setSearchBar()
        getAnimalArray()
        
        settingColors()
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationItem.backBarButtonItem?.tintColor = UIColor(red: 0/255.0, green: 144/255.0, blue: 81/255.0, alpha: 1.0)
    }

    
    override func viewWillAppear(_ animated: Bool) {
        if let index = self.tableView.indexPathForSelectedRow {
            self.tableView.deselectRow(at: index, animated: true)
        }
    }
    
    private func settingColors() {
        
        if #available(iOS 13.0, *) {
            collectionView.backgroundColor = UIColor.systemBackground
        } else {
            collectionView.backgroundColor = UIColor.white
        }
        
        if #available(iOS 13.0, *) {
            view.backgroundColor = UIColor.systemBackground
        } else {
            view.backgroundColor = UIColor.white
        }
    }
}


extension GameViewController {
    
    private func getCategories() {
        small = myGameStorage.filterByCategory(0)
        extraLight = myGameStorage.filterByCategory(1)
        light = myGameStorage.filterByCategory(2)
        medium = myGameStorage.filterByCategory(3)
        large = myGameStorage.filterByCategory(4)
        veryLarge = myGameStorage.filterByCategory(5)
        heavy = myGameStorage.filterByCategory(6)
        superHeavy = myGameStorage.filterByCategory(7)
        dangerous = myGameStorage.filterByCategory(8)
        
        switch index {
        case 0:
            filteredByCategory = small
        case 1:
            filteredByCategory = extraLight
        case 2:
            filteredByCategory = light
        case 3:
            filteredByCategory = medium
        case 4:
            filteredByCategory = large
        case 5:
            filteredByCategory = veryLarge
        case 6:
            filteredByCategory = heavy
        case 7:
            filteredByCategory = superHeavy
        case 8:
            filteredByCategory = dangerous
        default:
            break
        }
    }
    
    private func setSearchBar() {
        searchBar.delegate = self
        searchBar.returnKeyType = UIReturnKeyType.done
        searchBar.placeholder = "Search"
        navigationItem.titleView = searchBar
    }
    
    private func getAnimalArray() {
        myGameStorage.getArray {
            [weak self] in
            DispatchQueue.main.async {
                self?.getCategories()
                self?.tableView.reloadData()
                self?.isArrayArrived = true
            }
        }
        if !isArrayArrived {
            getCategories()
            tableView.reloadData()
            isArrayArrived = true
        }
    }
}


extension GameViewController {
    
    func setupKeyboardNotificationHandlers() {
        let nc = NotificationCenter.default
        
        nc.addObserver(forName: UIResponder.keyboardWillShowNotification, object: nil, queue: .main, using: keyboardWillShowNotificationHandler)
        
        nc.addObserver(forName: UIResponder.keyboardDidHideNotification, object: nil, queue: .main, using: keyboardDidHideNotificationHandler)
        
    }
    
    @objc func keyboardWillShowNotificationHandler(_ notification: Notification) {
        
        if let userInfo = notification.userInfo {
            if let frameValue = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
                let frame = frameValue.cgRectValue
                let keyboardVisibleHeight = frame.size.height
                
                self.tableView.contentInset.bottom = keyboardVisibleHeight
            }
        }
    }
    
    @objc func keyboardDidHideNotificationHandler(_ notification: Notification) {
        self.tableView.contentInset.bottom = 0
    }
}


extension GameViewController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cellTitles.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: GameCategoryCell = collectionView.dequeueReusableCell(withReuseIdentifier: GameCategoryCell.reuseIdentifier, for: indexPath) as! GameCategoryCell
        //cell.populate(with: UIImage(named: "\(indexPath.row).pdf")!, and: cellTitles[indexPath.row])
        cell.populate(with: UIImage(named: "\(indexPath.row)")!, and: cellTitles[indexPath.row])
        return cell
    }
}


extension GameViewController: UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
      
        searchBar.text = nil
        searchActive = false
        
        if indexPath.row == 0 {
            index = 0
            filteredByCategory = small
            headerLabel = cellTitles[0]
        }
        else if indexPath.row == 1 {
            index = 1
            filteredByCategory = extraLight
            headerLabel = cellTitles[1]
        } else if indexPath.row == 2 {
            index = 2
            filteredByCategory = light
            headerLabel = cellTitles[2]
        } else if indexPath.row == 3 {
            index = 3
            filteredByCategory = medium
            headerLabel = cellTitles[3]
        } else if indexPath.row == 4 {
            index = 4
            filteredByCategory = large
            headerLabel = cellTitles[4]
        } else if indexPath.row == 5 {
            index = 5
            filteredByCategory = veryLarge
            headerLabel = cellTitles[5]
        } else if indexPath.row == 6 {
            index = 6
            filteredByCategory = heavy
            headerLabel = cellTitles[6]
        } else if indexPath.row == 7 {
            index = 7
            filteredByCategory = superHeavy
            headerLabel = cellTitles[7]
        } else if indexPath.row == 8 {
            index = 8
            filteredByCategory = dangerous
            headerLabel = cellTitles[8]
        }
        tableView.reloadData()
    }
}


extension GameViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
//        if self.view.traitCollection.horizontalSizeClass == UIUserInterfaceSizeClass.regular {
//            return CGSize(width: (self.collectionView.frame.width) / 3, height: self.collectionView.frame.height / 6 * 5)
//        } else {
//            return CGSize(width: (self.collectionView.frame.width - 20) / 3, height: self.collectionView.frame.height / 6 * 5)
//        }
        return CGSize(width: (self.collectionView.frame.width - 20) / 3, height: self.collectionView.frame.height / 6 * 5)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
//        if self.view.traitCollection.horizontalSizeClass == UIUserInterfaceSizeClass.regular {
//            return 0
//        } else {
//            return 10
//        }
        return 10
    }
}


extension GameViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if (searchActive) {
            if filteredGames.count > 0 {
                return filteredGames.count
            } else {
                return 0
            }
        } else {
            return filteredByCategory.count
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: GameCell = tableView.dequeueReusableCell(withIdentifier: GameCell.reuseIdentifier, for: indexPath) as! GameCell
        //
        if searchActive && filteredGames.count > 0 {
            let sortedFilteredGames = filteredGames.sorted(by: { $0.name < $1.name })
            let cc = sortedFilteredGames[indexPath.row]
            cell.setAccessoryView()
            cell.configure(with: cc)
        } else {
            let sortedGames = filteredByCategory.sorted(by: { $0.name < $1.name })
            let cc = sortedGames[indexPath.row]
            cell.setAccessoryView()
            cell.configure(with: cc)
        }
        return cell
    }
}


extension GameViewController: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true
        searchBar.returnKeyType = .done
        searchBar.setShowsCancelButton(true, animated: true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
        searchBar.resignFirstResponder()
        searchBar.text = nil
        searchBar.setShowsCancelButton(false, animated: true)
        tableView.reloadData()
    }
    
     //ovo se desava na klik na Done
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = true
        searchBar.resignFirstResponder()
        searchBar.setShowsCancelButton(false, animated: true)
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        filteredGames = []
        let searchT = searchText.lowercased()
        
        for game in filteredByCategory {
            
            let gameName = game.name.lowercased()
            
            if gameName.contains(searchT) {
                filteredGames.append(game)
            } else { continue }
        }
        
        if (filteredGames.count == filteredByCategory.count) || searchT.count == 0 {
            searchActive = false
        } else {
            searchActive = true
        }
        tableView.reloadData()
    }
}


extension GameViewController: UITableViewDelegate {
    
    private func showGameDetails(game: GameSpecific) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        guard let vc = storyboard.instantiateViewController(withIdentifier: "GameDetailController") as? GameDetailController else {
            fatalError("could not find GameDetailController")
        }
        vc.game = game
        show(vc, sender: self)
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let sortedGames = filteredByCategory.sorted(by: { $0.name < $1.name })
        var selectedGame = GameSpecific()
        
        if searchActive && filteredGames.count > 0 {
            let sortedFilteredGames = filteredGames.sorted(by: { $0.name < $1.name })
            selectedGame = sortedFilteredGames[indexPath.row]
        } else {
            selectedGame = sortedGames[indexPath.row]
        }
        showGameDetails(game: selectedGame)
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let vw = UIView(frame: CGRect(x:0, y:0, width:tableView.frame.size.width, height:18))
        let lineFrame = CGRect(x: 16, y: 12, width: (tableView.bounds.size.width - 32), height: 1)
        let line = UIView(frame: lineFrame)
        line.backgroundColor = UIColor.orange
        vw.addSubview(line)
        let label = PaddingLabel(frame: CGRect(x:0, y:0, width:tableView.frame.size.width, height:2*(line.frame.origin.y)))
        label.center = vw.center
        if self.view.traitCollection.horizontalSizeClass == UIUserInterfaceSizeClass.regular {
            label.font = UIFont(name: "Avenir-Heavy", size: 20.0)
        } else {
            label.font = UIFont(name: "Avenir-Heavy", size: 17.0)
        }
        if #available(iOS 13.0, *) {
           label.textColor = UIColor.label
           label.backgroundColor = UIColor.systemBackground
        } else {
            label.textColor = UIColor.black
            label.backgroundColor = UIColor.white
        }
        label.text = headerLabel
        label.frame.origin.y = 0
        label.frame.size.width = label.intrinsicContentSize.width
        label.frame.origin.x = tableView.bounds.size.width/2 - label.frame.size.width/2
        //label.translatesAutoresizingMaskIntoConstraints = false - kada se ovo odkomentarise, desava se onaj problem sa pomeranjem table hedera u levo!!!
        //label.backgroundColor = UIColor.white
        vw.addSubview(label)
        if #available(iOS 13.0, *) {
                                  vw.backgroundColor = UIColor.systemBackground
                             } else {
                                  vw.backgroundColor = UIColor.white
                             }
        //vw.backgroundColor = UIColor.white
        
        return vw
    }
}


