//
//  PossibleGamesController.swift
//  TrueBullet
//
//  Created by Milica on 12/6/18.
//  Copyright © 2018 Milica Cokic. All rights reserved.
//

import UIKit

final class PossibleGamesController: UIViewController, UITableViewDelegate {
    
    
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var upperView: UIView!
    
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var typeLabel: UILabel!
    @IBOutlet private weak var weightLabel: UILabel!
    @IBOutlet private weak var manufacturerLabel: UILabel!
    @IBOutlet private weak var category: UILabel!
    @IBOutlet private weak var segmentedControl: UISegmentedControl!
    @IBOutlet private weak var manufacturer: UILabel!
    @IBOutlet private weak var caliber: UILabel!
    
    @IBOutlet private weak var constraintUp: NSLayoutConstraint?
    @IBOutlet private weak var constraintDown: NSLayoutConstraint?
    @IBOutlet private weak var segmentedHeight: NSLayoutConstraint?
    @IBOutlet private weak var usageTextTop: NSLayoutConstraint?
    @IBOutlet private weak var topConstraint: NSLayoutConstraint!
    @IBOutlet private weak var middleConstraint: NSLayoutConstraint?
    @IBOutlet private weak var bottomConstraint: NSLayoutConstraint?
    
    
    var previousOffset: CGFloat = 0
    var previousHeight: CGFloat = 0
    
    var upperHeight: CGFloat = 0
    var headerViewMaxHeight: CGFloat = 0
    let headerViewMinHeight: CGFloat = 0 + UIApplication.shared.statusBarFrame.height
    
    var bullet: Bullet?
    private var categoryText: String?
    
    lazy var searchBar = UISearchBar(frame: CGRect.zero)
    private var searchActive: Bool = false
    
    private var index: Int = 0
    private var isArrayArrived = false
    private var application: String = "Application"
    
    private var heightConstraint: NSLayoutConstraint?
   
    private var myGameStorage = GameStorage.shared
    private var allAnimalsForBullet: [GameSpecific] = []
    private var animalsFiltered: [GameSpecific] = []
    private var specificGames: [GameSpecific] = []

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        
        setupKeyboardNotificationHandlers()
        setSearchBar()
        getPossibleGamesArray()
    
        configureData()
        setSegmentedControl()
        
        settingColors()
        
        tableView.alwaysBounceVertical = false
        
        //print("Visina upper view-a je: \(upperView.frame.size.height)")
        //heightConstraint = upperView.heightAnchor.constraint(equalToConstant: 400)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("Visina upper view: \(upperView.frame.size.height)")
        //ovo ne mora da se radi jer ove konstante vec jesu nula u storyboardu
//        if bullet?.manufacturer == nil {
//            constraintUp?.constant = 0
//            constraintDown?.constant = 0
//            usageTextTop?.constant = 0
//        }
    }
    
    
    private func settingColors() {
           
          if #available(iOS 13.0, *) {
              upperView.backgroundColor = UIColor.systemBackground
              view.backgroundColor = UIColor.systemBackground
          } else {
              upperView.backgroundColor = UIColor.white
              view.backgroundColor = UIColor.white
          }
     }
    
    
    private func setSegmentedControl() {
        if self.view.traitCollection.horizontalSizeClass == UIUserInterfaceSizeClass.regular {
            segmentedHeight?.constant = 35
            let basicFont = UIFont.systemFont(ofSize: 18)
            let font = UIFont(name: "Avenir", size: 18.0)
            segmentedControl.setTitleTextAttributes([NSAttributedString.Key.font: font ?? basicFont], for: .normal)
        } else {
            segmentedHeight?.constant = 28
            let basicFont = UIFont.systemFont(ofSize: 13)
            let font = UIFont(name: "Avenir", size: 13.0)
            segmentedControl.setTitleTextAttributes([NSAttributedString.Key.font: font ?? basicFont], for: .normal)
        }
    }
    
    
    private func configureData() {
        //ovaj ceo view moze da bude zasebna klasa, zasebna komponenta, kao sto smo radili u Valutama
        nameLabel.text = bullet?.name
        if let manufacturer = bullet?.manufacturer {
            manufacturerLabel.text = manufacturer
        } else {
            manufacturer.isHidden = true
            manufacturerLabel.isHidden = true
            manufacturer.heightAnchor.constraint(equalToConstant: 0).isActive = true
            manufacturerLabel.heightAnchor.constraint(equalToConstant: 0).isActive = true
        }
        
        typeLabel.text = bullet?.type
        if let weight = bullet?.weight {
            var shortWeight = 1
            if !(weight - floor(weight) > 0.000001) {
                shortWeight = Int(weight)
                weightLabel.text = "\(String(describing: shortWeight)) gr"
            } else {
                weightLabel.text = "\(String(describing: weight)) gr"
            }
        }
        
        if let slugWeight = bullet?.slugWeight {
            weightLabel.text = slugWeight
            manufacturer.text = "Shell Length:"
            caliber.text = "Gauge:"
        }
        let boldText = "Application: "
        var attrs = [NSAttributedString.Key.font: UIFont(name: "Avenir-Heavy", size: 17.0), NSAttributedString.Key.foregroundColor: UIColor.black]
        
        if #available(iOS 13.0, *) {
             if self.view.traitCollection.horizontalSizeClass == UIUserInterfaceSizeClass.regular {
                 attrs = [NSAttributedString.Key.font: UIFont(name: "Avenir-Heavy", size: 22.0), NSAttributedString.Key.foregroundColor: UIColor.label]
             } else {
                 attrs = [NSAttributedString.Key.font: UIFont(name: "Avenir-Heavy", size: 17.0), NSAttributedString.Key.foregroundColor: UIColor.label]
             }
        } else {
           if self.view.traitCollection.horizontalSizeClass == UIUserInterfaceSizeClass.regular {
                 attrs = [NSAttributedString.Key.font: UIFont(name: "Avenir-Heavy", size: 22.0), NSAttributedString.Key.foregroundColor: UIColor.black]
             } else {
                 attrs = [NSAttributedString.Key.font: UIFont(name: "Avenir-Heavy", size: 17.0), NSAttributedString.Key.foregroundColor: UIColor.black]
             }
        }
        
       
        let attributedString = NSMutableAttributedString(string:boldText, attributes:attrs as [NSAttributedString.Key : Any])
        
        let normalText = categoryText
        guard let normal = normalText else { return }
        var attrs2 = [NSAttributedString.Key.font: UIFont(name: "Avenir", size: 17.0), NSAttributedString.Key.foregroundColor: UIColor.black]
        
        if #available(iOS 13.0, *) {
              if self.view.traitCollection.horizontalSizeClass == UIUserInterfaceSizeClass.regular {
                        attrs2 = [NSAttributedString.Key.font: UIFont(name: "Avenir", size: 22.0), NSAttributedString.Key.foregroundColor: UIColor.label]
                    } else {
                        attrs2 = [NSAttributedString.Key.font: UIFont(name: "Avenir", size: 17.0), NSAttributedString.Key.foregroundColor: UIColor.label]
                    }
        } else {
           if self.view.traitCollection.horizontalSizeClass == UIUserInterfaceSizeClass.regular {
                      attrs2 = [NSAttributedString.Key.font: UIFont(name: "Avenir", size: 22.0), NSAttributedString.Key.foregroundColor: UIColor.black]
                  } else {
                      attrs2 = [NSAttributedString.Key.font: UIFont(name: "Avenir", size: 17.0), NSAttributedString.Key.foregroundColor: UIColor.black]
                  }
        }
        
       
        let normalString = NSMutableAttributedString(string: normal, attributes:attrs2 as [NSAttributedString.Key : Any])
        

        attributedString.append(normalString)
        category.lineBreakMode = .byWordWrapping
        category.numberOfLines = 0
        
        category.attributedText = attributedString
    }
    
    private func setSearchBar() {
        searchBar.delegate = self
        searchBar.returnKeyType = UIReturnKeyType.done
        searchBar.placeholder = "Search"
        navigationItem.titleView = searchBar
    }
    
    private func getPossibleGamesArray() {
        myGameStorage.getArray {
            [weak self] in
            DispatchQueue.main.async {
                
                self?.specificGames = (self?.myGameStorage.specificGames)!
                
                if self?.index == 0 {
                    self?.allAnimalsForBullet = (self?.usualAnimalsForBullet())!
                } else if self?.index == 1 {
                    self?.allAnimalsForBullet = (self?.possibleAnimalsForBullet())!
                }
                self?.tableView.reloadData()
                self?.isArrayArrived = true
            }
        }
        
        if !isArrayArrived {
            specificGames = myGameStorage.specificGames
            if index == 0 {
                allAnimalsForBullet = usualAnimalsForBullet()
            } else if index == 1 {
                allAnimalsForBullet = possibleAnimalsForBullet()
            }
            tableView.reloadData()
            isArrayArrived = true
        }
    }
    
    @IBAction func indexChanged(_ sender: UISegmentedControl) {
        searchActive = false
        searchBar.resignFirstResponder()
        searchBar.text = nil
        searchBar.setShowsCancelButton(false, animated: true)
        switch segmentedControl.selectedSegmentIndex
        {
        case 0:
            allAnimalsForBullet = usualAnimalsForBullet()
            tableView.reloadData()
        case 1:
            allAnimalsForBullet = possibleAnimalsForBullet()
            tableView.reloadData()
        default:
            break
        }
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {

        //uvek krece od nule, tj. scrollView.contentOffset.y je u startu nula
         scrollView.bounces = false
         topConstraint?.constant = scrollView.contentOffset.y
         //scrollView.alwaysBounceVertical = false
    }
    
    
    
    private func usualAnimalsForBullet() -> [GameSpecific] {
        
        var usualAnimals: [GameSpecific] = []
        guard let selectedBullet = bullet else {return []}
        guard let minChok = bullet?.chok0 else {return []}
        
        if minChok >= 7 && minChok < 30 {
            categoryText = "Minimum standard for small game under 10 lbs. Also useful for very small game."
            for game in myGameStorage.specificGames {
                if let weight = game.weightMF {
                    if weight > 0.5 && weight < 10 {
                        game.tagMF = .MF
                        usualAnimals.append(game)
                    }
                }
                if let weightMale = game.weightM, let weightFem = game.weightF {
                    if weightMale > 0.5 && weightMale < 10 && weightFem > 0.5 && weightFem < 10 {
                        game.tagMF = .MF
                        usualAnimals.append(game)
                    }
                }
            }
            
        } else if minChok >= 30 && minChok < 88 {
            //categoryText = "Best suited for very light big game between 10 and 40 lbs. Can also be used for small game with weight less than 10 lbs."
            categoryText = "Minimum standard for very light big game between 10 and 40 lbs and useful for small game under 10 lbs."
            for game in specificGames {
                if let weight = game.weightMF {
                    if weight >= 10 && weight < 40 {
                        game.tagMF = .MF
                        usualAnimals.append(game)
                    }
                }
                if let weightMale = game.weightM, let weightFem = game.weightF {
                    if weightMale >= 10 && weightMale < 40 && weightFem >= 10 && weightFem < 40 {
                        game.tagMF = .MF
                        usualAnimals.append(game)
                    }
                    else if weightFem > 0.5 && weightFem < 10 && weightMale >= 10 && weightMale < 40 {
                        game.tagMF = .MF
                        usualAnimals.append(game)
                    }
                }
            }
        } else if minChok >= 88 && minChok < 270 {
           //categoryText = "Best suited for light big game between 40 and 110 lbs. Can also be used for very light big game between 10 and 40 lbs."
            categoryText = "Minimum standard for light big game between 40 and 110 lbs and useful for very light big game between 10 and 40 lbs."
            for game in specificGames {
                guard let predator = game.predator else {return []}
                if let weight = game.weightMF {
                    if weight >= 40 && weight < 110 && !predator {
                        game.tagMF = .MF
                        usualAnimals.append(game)
                    }
                }
                if let weightMale = game.weightM, let weightFem = game.weightF {
                    if weightMale >= 40 && weightMale < 110 && weightFem >= 40 && weightFem < 110 && !predator {
                        game.tagMF = .MF
                        usualAnimals.append(game)
                    }
                    else if weightFem < 40 && weightMale >= 40 && weightMale < 110 {
                        game.tagMF = .MF
                        usualAnimals.append(game)
                    }
                }
            }
        } else if minChok >= 270 && minChok < 600 {
            //categoryText = "Best suited for medium big game between 110 and 330 lbs."
            categoryText = "Minimum standard for medium big game between 110 and 330 lbs and useful for light big game between 40 and 110 lbs."
            for game in specificGames {
                guard let predator = game.predator else {return []}
                if let weight = game.weightMF {
                    if weight >= 110 && weight < 330 && !predator {
                        game.tagMF = .MF
                        usualAnimals.append(game)
                    }
                }
                if let weightMale = game.weightM, let weightFem = game.weightF {
                    if weightMale >= 110 && weightMale < 330 && weightFem >= 110 && weightFem < 330 && !predator {
                        game.tagMF = .MF
                        usualAnimals.append(game)
                    }
                    else if weightFem < 110 && weightMale >= 110 && weightMale < 330 && !predator {
                        game.tagMF = .MF
                        usualAnimals.append(game)
                    }
                    else if weightMale >= 110 && weightFem >= 40 && weightFem < 110 && !predator {
                        game.tagMF = .MF
                        usualAnimals.append(game)
                    }
                }
            }
            
        } else if minChok >= 600 && minChok < 1000 {
            categoryText = "Minimum standard for large big game between 330 and 770 lbs and medium dangerous predators between 110 and 330 lbs. Also useful for medium big game between 110 and 330 lbs."
            for game in specificGames {
                guard let predator = game.predator else {return []}
                if let weight = game.weightMF {
                    if weight >= 330 && weight < 770 && !predator {
                        game.tagMF = .MF
                        usualAnimals.append(game)
                    }
                    if weight >= 110 && weight < 330 && predator {
                        if selectedBullet.type != "fmj rn" && selectedBullet.type != "Barnes solid" && selectedBullet.type != "solid rn" && selectedBullet.type != "DGS" && selectedBullet.type != "Sledgehammer Solid" {
                            game.tagMF = .MF
                            usualAnimals.append(game)
                        }
                    }
                }
                if let weightMale = game.weightM, let weightFem = game.weightF {
                    if weightMale >= 330 && weightMale < 770 && weightFem >= 330 && weightFem < 770 && !predator {
                        game.tagMF = .MF
                        usualAnimals.append(game)
                    }
                    else if weightFem < 330 && weightMale >= 330 && weightMale < 770 && !predator {
                        game.tagMF = .MF
                        usualAnimals.append(game)
                    }
                    else if weightMale >= 110 && weightMale < 330 && weightFem >= 110 && weightFem < 330 && predator {
                        if selectedBullet.type != "fmj rn" && selectedBullet.type != "Barnes solid" && selectedBullet.type != "solid rn" && selectedBullet.type != "DGS" && selectedBullet.type != "Sledgehammer Solid" {
                            game.tagMF = .MF
                            usualAnimals.append(game)
                        }
                    }
                    else if weightFem < 110 && weightMale >= 110 && weightMale < 330 && predator {
                        if selectedBullet.type != "fmj rn" && selectedBullet.type != "Barnes solid" && selectedBullet.type != "solid rn" && selectedBullet.type != "DGS" && selectedBullet.type != "Sledgehammer Solid" {
                            game.tagMF = .MF
                            usualAnimals.append(game)
                        }
                    }
                    else if weightFem >= 110 && weightFem < 330 && weightMale >= 330 && !predator {
                        game.tagMF = .MF
                        usualAnimals.append(game)
                    }
                    else if weightFem >= 40 && weightFem < 110 && weightMale >= 110 && predator {
                        if selectedBullet.type != "fmj rn" && selectedBullet.type != "Barnes solid" && selectedBullet.type != "solid rn" && selectedBullet.type != "DGS" && selectedBullet.type != "Sledgehammer Solid" {
                            game.tagMF = .MF
                            usualAnimals.append(game)
                        }
                    }
                }
            }
        } else if minChok >= 1000 && minChok < 1650 {
            categoryText = "Minimum standard for very large big game between 770 and 1540 lbs and large dangerous predators between 330 and 770 lbs. Also useful for large big game between 330 and 770 lbs and medium dangerous predators between 110 and 330 lbs."
            for game in specificGames {
            guard let predator = game.predator else {return []}
                if let weight = game.weightMF {
                    if weight >= 770 && weight < 1540 && !predator {
                        game.tagMF = .MF
                        usualAnimals.append(game)
                        print()
                    }
                    if weight >= 330 && weight < 770 && predator {
                        if selectedBullet.type != "fmj rn" && selectedBullet.type != "Barnes solid" && selectedBullet.type != "solid rn" && selectedBullet.type != "DGS" && selectedBullet.type != "Sledgehammer Solid" {
                            game.tagMF = .MF
                            usualAnimals.append(game)
                        }
                    }
                }
                if let weightMale = game.weightM, let weightFem = game.weightF {
                    if weightMale >= 770 && weightMale < 1540 && weightFem >= 770 && weightFem < 1540 && !predator {
                        game.tagMF = .MF
                        usualAnimals.append(game)
                    }
                    else if weightFem < 770 && weightMale >= 770 && weightMale < 1540 && !predator {
                            game.tagMF = .MF
                            usualAnimals.append(game)
                    }
                    else if weightMale >= 330 && weightMale < 770 && weightFem >= 330 && weightFem < 770 && predator {
                            if selectedBullet.type != "fmj rn" && selectedBullet.type != "Barnes solid" && selectedBullet.type != "solid rn" && selectedBullet.type != "DGS" && selectedBullet.type != "Sledgehammer Solid" {
                                game.tagMF = .MF
                                usualAnimals.append(game)
                            }
                        }
                    else if weightFem < 330 && weightMale >= 330 && weightMale < 770 && predator {
                        if selectedBullet.type != "fmj rn" && selectedBullet.type != "Barnes solid" && selectedBullet.type != "solid rn" && selectedBullet.type != "DGS" && selectedBullet.type != "Sledgehammer Solid" {
                                game.tagMF = .MF
                                usualAnimals.append(game)
                        }
                    }
                    else if weightFem >= 110 && weightFem < 330 && weightMale >= 330 && predator {
                        if selectedBullet.type != "fmj rn" && selectedBullet.type != "Barnes solid" && selectedBullet.type != "solid rn" && selectedBullet.type != "DGS" && selectedBullet.type != "Sledgehammer Solid" {
                            game.tagMF = .MF
                            usualAnimals.append(game)
                        }
                    }
                    else if weightMale >= 770 && weightFem >= 330 && weightFem < 770 && !predator {
                        game.tagMF = .MF
                        usualAnimals.append(game)
                    }
                }
            }
        } else if minChok >= 1650 && minChok < 2800 {
            if selectedBullet.type != "fmj rn" && selectedBullet.type != "Barnes solid" && selectedBullet.type != "solid rn" && selectedBullet.type != "DGS" && selectedBullet.type != "Sledgehammer Solid" {
                categoryText = "Minimum standard for heavy game between 1540 and 3100 lbs and very large dangerous predators between 770 and 1540 lbs. Also useful for very large big game between 770 and 1540 lbs and dangerous predators between 110 and 770 lbs."
            } else {
                categoryText = "Minimum standard for heavy game between 1540 and 3100 lbs. Also useful for very large big game between 770 and 1540 lbs and even for super heavy game over 3100 lbs."
            }
            for game in specificGames {
             guard let predator = game.predator else {return []}
                if let weight = game.weightMF {
                    if weight >= 1540 && weight < 3100 {
                        game.tagMF = .MF
                        usualAnimals.append(game)
                    }
                    if weight >= 770 && weight < 1540 && predator {
                        if selectedBullet.type != "fmj rn" && selectedBullet.type != "Barnes solid" && selectedBullet.type != "solid rn" && selectedBullet.type != "DGS" && selectedBullet.type != "Sledgehammer Solid" {
                            game.tagMF = .MF
                            usualAnimals.append(game)
                        }
                    }
                }
                if let weightMale = game.weightM, let weightFem = game.weightF {
                    if weightMale >= 1540 && weightMale < 3100 && weightFem >= 1540 && weightFem < 3100 {
                        game.tagMF = .MF
                        usualAnimals.append(game)
                    }
                    else if weightFem < 1540 && weightMale >= 1540 && weightMale < 3100 {
                        game.tagMF = .MF
                        usualAnimals.append(game)
                    }
                    else if weightMale >= 770 && weightMale < 1540 && weightFem >= 770 && weightFem < 1540 && predator {
                        if selectedBullet.type != "fmj rn" && selectedBullet.type != "Barnes solid" && selectedBullet.type != "solid rn" && selectedBullet.type != "DGS" && selectedBullet.type != "Sledgehammer Solid" {
                            game.tagMF = .MF
                            usualAnimals.append(game)
                        }
                    }
                    else if weightFem < 770 && weightMale >= 770 && weightMale < 1540 && predator {
                        if selectedBullet.type != "fmj rn" && selectedBullet.type != "Barnes solid" && selectedBullet.type != "solid rn" && selectedBullet.type != "DGS" && selectedBullet.type != "Sledgehammer Solid" {
                            game.tagMF = .MF
                            usualAnimals.append(game)
                        }
                    }
                    else if weightFem >= 330 && weightFem < 770 && weightMale >= 770 && predator {
                        if selectedBullet.type != "fmj rn" && selectedBullet.type != "Barnes solid" && selectedBullet.type != "solid rn" && selectedBullet.type != "DGS" && selectedBullet.type != "Sledgehammer Solid" {
                            game.tagMF = .MF
                            usualAnimals.append(game)
                        }
                    }
                    else if weightMale >= 1540 && weightFem >= 770 && weightFem < 1540 {
                        game.tagMF = .MF
                        usualAnimals.append(game)
                    }
                }
            }
        } else if minChok >= 2800 {
            if selectedBullet.type != "fmj rn" && selectedBullet.type != "Barnes solid" && selectedBullet.type != "solid rn" && selectedBullet.type != "DGS" && selectedBullet.type != "Sledgehammer Solid" {
                categoryText = "Minimum standard for super heavy game weighing 3100 lbs and more. Also useful for heavy game between 1540 and 3100 lbs and dangerous predators between 330 and 1540 lbs."
            } else {
                categoryText = "Minimum standard for super heavy game weighing 3100 lbs and more. Also useful for heavy game between 1540 and 3100 lbs."
            }
            for game in specificGames {
                if let weight = game.weightMF {
                    if weight >= 3100 {
                        game.tagMF = .MF
                        usualAnimals.append(game)
                    }
                }
                if let weightMale = game.weightM, let weightFem = game.weightF {
                    if weightMale >= 3100 && weightFem >= 3100 {
                        game.tagMF = .MF
                        usualAnimals.append(game)
                    }
                    else if weightMale >= 3100 && weightFem < 3100 {
                        game.tagMF = .MF
                        usualAnimals.append(game)
                    }
                    else if weightMale >= 3100 && weightFem >= 1540 && weightFem < 3100 {
                        game.tagMF = .MF
                        usualAnimals.append(game)
                    }
                }
            }
        }
        return usualAnimals
    }
    
    
    private func possibleAnimalsForBullet() -> [GameSpecific] {
        
        var possibleAnimals: [GameSpecific] = []
        guard let minChok = bullet?.chok0 else {return []}
        guard let selectedBullet = bullet else {return []}
        
        if minChok >= 7 && minChok < 30 {
            for game in specificGames {
                if game.name == "Very small game like squirrels or rats" {
                    possibleAnimals.append(game)
                }
            }
        }
         if minChok >= 30 && minChok < 88 {
            for game in specificGames {
                guard let predator = game.predator else { return [] }
                if let weight = game.weightMF {
                    if weight > 0.5 && weight < 10 {
                        game.tagMF = .MF
                        possibleAnimals.append(game)
                    }
                }
                if let weightMale = game.weightM, let weightFem = game.weightF {
                    if weightMale > 0.5 && weightMale < 10 && weightFem > 0.5 && weightFem < 10 {
                        game.tagMF = .MF
                        possibleAnimals.append(game)
                    }
                    else if weightFem >= 10 && weightFem < 40 && weightMale >= 40 && !predator {
                        game.tagMF = .F
                        possibleAnimals.append(game)
                    }
                }
            }
        }
        if minChok >= 88 && minChok < 270 {
            for game in specificGames {
                guard let predator = game.predator else { return [] }
                if let weight = game.weightMF {
                    if weight >= 10 && weight < 40 {
                        game.tagMF = .MF
                        possibleAnimals.append(game)
                    }
                }
                if let weightMale = game.weightM, let weightFem = game.weightF {
                    if weightMale >= 10 && weightMale < 40 && weightFem >= 10 && weightFem < 40 {
                        game.tagMF = .MF
                        possibleAnimals.append(game)
                    }
                    else if weightFem > 0.5 && weightFem < 10 && weightMale >= 10 && weightMale < 40 {
                        game.tagMF = .MF
                        possibleAnimals.append(game)
                    }
                    else if weightFem >= 40 && weightFem < 110 && weightMale >= 110 && !predator {
                        game.tagMF = .F
                        possibleAnimals.append(game)
                    }
                }
            }
        }  else if minChok >= 270 && minChok < 600 {
            for game in specificGames {
                guard let predator = game.predator else { return [] }
                if let weight = game.weightMF {
                    if weight >= 40 && weight < 110 && !predator {
                        game.tagMF = .MF
                        possibleAnimals.append(game)
                    }
                }
                if let weightMale = game.weightM, let weightFem = game.weightF {
                    
                    if weightMale >= 40 && weightMale < 110 && weightFem >= 40 && weightFem < 110 && !predator {
                        game.tagMF = .MF
                        possibleAnimals.append(game)
                    }
                    else if weightFem < 40 && weightMale >= 40 && weightMale < 110 && !predator {
                        game.tagMF = .MF
                        possibleAnimals.append(game)
                    }

                    else if weightFem >= 110 && weightFem < 330 && weightMale >= 330 && !predator {
                        game.tagMF = .F
                        possibleAnimals.append(game)
                    } // dodato iz usual
                }
            }
            
        } else if minChok >= 600 && minChok < 1000 {
            for game in specificGames {
                guard let predator = game.predator else { return [] }
                if let weight = game.weightMF {
                    if weight >= 110 && weight < 330 && !predator {
                        game.tagMF = .MF
                        possibleAnimals.append(game)
                    }
                }
                if let weightMale = game.weightM, let weightFem = game.weightF {
                    if weightMale >= 110 && weightMale < 330 && weightFem >= 110 && weightFem < 330 && !predator {
                        game.tagMF = .MF
                        possibleAnimals.append(game)
                    }
                    else if weightFem < 110 && weightMale >= 110 && weightMale < 330 && !predator {
                        game.tagMF = .MF
                        possibleAnimals.append(game)
                    }
                    else if weightFem >= 330 && weightFem < 770 && weightMale >= 770 && !predator {
                        game.tagMF = .F
                        possibleAnimals.append(game)
                    } // dodato iz usual
                }
            }
        } else if minChok >= 1000 && minChok < 1650 {
            for game in specificGames {
                guard let predator = game.predator else { return [] }
                if let weight = game.weightMF {
                    if weight >= 330 && weight < 770 && !predator {
                        game.tagMF = .MF
                        possibleAnimals.append(game)
                    }
                    if weight >= 110 && weight < 330 && predator {
                        if selectedBullet.type != "fmj rn" && selectedBullet.type != "Barnes solid" && selectedBullet.type != "solid rn" && selectedBullet.type != "DGS" && selectedBullet.type != "Sledgehammer Solid" {
                            game.tagMF = .MF
                            possibleAnimals.append(game)
                        }
                    }
                    if weight >= 1540 && weight < 3100 && !predator {
                        if selectedBullet.type == "fmj rn" || selectedBullet.type == "Barnes solid" || selectedBullet.type == "solid rn" || selectedBullet.type == "DGS" || selectedBullet.type == "Sledgehammer Solid" {
                            game.tagMF = .MF
                            possibleAnimals.append(game)
                        }
                    }
                }
                if let weightMale = game.weightM, let weightFem = game.weightF {
                    if weightMale >= 330 && weightMale < 770 && weightFem >= 330 && weightFem < 770 && !predator {
                        game.tagMF = .MF
                        possibleAnimals.append(game)
                    }
                    else if weightFem < 330 && weightMale >= 330 && weightMale < 770 && !predator {
                        game.tagMF = .MF
                        possibleAnimals.append(game)
                    }
                    else if weightFem >= 1540 && weightFem < 3100 && weightMale >= 1540 && weightMale < 3100 && !predator {
                        if selectedBullet.type == "fmj rn" || selectedBullet.type == "Barnes solid" || selectedBullet.type == "solid rn" || selectedBullet.type == "DGS" || selectedBullet.type == "Sledgehammer Solid" {
                            game.tagMF = .MF
                            possibleAnimals.append(game)
                        }
                    }
                    else if weightFem >= 770 && weightFem < 1540 && weightMale >= 1540 && !predator {
                        if selectedBullet.type == "fmj rn" || selectedBullet.type == "Barnes solid" || selectedBullet.type == "solid rn" || selectedBullet.type == "DGS" || selectedBullet.type == "Sledgehammer Solid" {
                             game.tagMF = .MF
                             possibleAnimals.append(game)
                        }
                        if selectedBullet.type != "fmj rn" && selectedBullet.type != "Barnes solid" && selectedBullet.type != "solid rn" && selectedBullet.type != "DGS" && selectedBullet.type != "Sledgehammer Solid" {
                            game.tagMF = .F
                            possibleAnimals.append(game)
                        }
                    }
                    if weightMale >= 110 && weightMale < 330 && weightFem >= 110 && weightFem < 330 && predator {
                        if selectedBullet.type != "fmj rn" && selectedBullet.type != "Barnes solid" && selectedBullet.type != "solid rn" && selectedBullet.type != "DGS" && selectedBullet.type != "Sledgehammer Solid" {
                            game.tagMF = .MF
                            possibleAnimals.append(game)
                        }
                    }
                    else if weightFem < 110 && weightMale >= 110 && weightMale < 330 && predator {
                        if selectedBullet.type != "fmj rn" && selectedBullet.type != "Barnes solid" && selectedBullet.type != "solid rn" && selectedBullet.type != "DGS" && selectedBullet.type != "Sledgehammer Solid" {
                            game.tagMF = .MF
                            possibleAnimals.append(game)
                        }
                    }
                }
            }
            
        } else if minChok >= 1650 && minChok < 2800 {
//            categoryText = "Appropriate for heavy big game with weight more than 1539 and less than 3100 lbs. Also appropriate for very large dangerous animals with weight more than 769 and less than 1540 lbs."
            for game in specificGames {
                guard let predator = game.predator else { return [] }
                if let weight = game.weightMF {
                    if weight >= 770 && weight < 1540 && !predator {
                        game.tagMF = .MF
                        possibleAnimals.append(game)
                    }
                    if weight >= 330 && weight < 770 && predator {
                        if selectedBullet.type != "fmj rn" && selectedBullet.type != "Barnes solid" && selectedBullet.type != "solid rn" && selectedBullet.type != "DGS" && selectedBullet.type != "Sledgehammer Solid" {
                            game.tagMF = .MF
                            possibleAnimals.append(game)
                        }
                    }
                    if weight >= 110 && weight < 330 && predator {
                        if selectedBullet.type != "fmj rn" && selectedBullet.type != "Barnes solid" && selectedBullet.type != "solid rn" && selectedBullet.type != "DGS" && selectedBullet.type != "Sledgehammer Solid" {
                            game.tagMF = .MF
                            possibleAnimals.append(game)
                        }
                    }
                    if weight > 3100 {
                        if selectedBullet.type == "fmj rn" || selectedBullet.type == "Barnes solid" || selectedBullet.type == "solid rn" || selectedBullet.type == "DGS" || selectedBullet.type == "Sledgehammer Solid" {
                            game.tagMF = .MF
                            possibleAnimals.append(game)
                        }
                    }
                }
                if let weightMale = game.weightM, let weightFem = game.weightF {
                    if weightMale >= 770 && weightMale < 1540 && weightFem >= 770 && weightFem < 1540 && !predator {
                        game.tagMF = .MF
                        possibleAnimals.append(game)
                    }
                    else if weightFem < 770 && weightMale >= 770 && weightMale < 1540 && !predator {
                        game.tagMF = .MF
                        possibleAnimals.append(game)
                    }
                    else if weightFem >= 1540 && weightFem < 3100 && weightMale >= 3100 && !predator {
                        // da li ovo za zenke treba ovako po novoj tatinoj teoriji?
                        if selectedBullet.type != "fmj rn" && selectedBullet.type != "Barnes solid" && selectedBullet.type != "solid rn" && selectedBullet.type != "DGS" && selectedBullet.type != "Sledgehammer Solid" {
                            game.tagMF = .F
                            possibleAnimals.append(game)
                        }// dodato iz usual
                        if selectedBullet.type == "fmj rn" || selectedBullet.type == "Barnes solid" || selectedBullet.type == "solid rn" || selectedBullet.type == "DGS" || selectedBullet.type == "Sledgehammer Solid" {
                            game.tagMF = .MF
                            possibleAnimals.append(game)
                        }
                    }
                    else if weightFem >= 3100 && weightMale >= 3100 {
                        if selectedBullet.type == "fmj rn" || selectedBullet.type == "Barnes solid" || selectedBullet.type == "solid rn" || selectedBullet.type == "DGS" || selectedBullet.type == "Sledgehammer Solid" {
                            game.tagMF = .MF
                            possibleAnimals.append(game)
                        }
                    }
                    if weightMale >= 330 && weightMale < 770 && weightFem >= 330 && weightFem < 770 && predator {
                        if selectedBullet.type != "fmj rn" && selectedBullet.type != "Barnes solid" && selectedBullet.type != "solid rn" && selectedBullet.type != "DGS" && selectedBullet.type != "Sledgehammer Solid" {
                            game.tagMF = .MF
                            possibleAnimals.append(game)
                        }
                    }
//                    else if weightFem >= 330 && weightFem < 770 && weightMale >= 770 && predator {
//                        game.tagMF = .F
//                        possibleAnimals.append(game)
//                    }
                    else if weightFem < 330 && weightMale >= 330 && weightMale < 770 && predator {
                        if selectedBullet.type != "fmj rn" && selectedBullet.type != "Barnes solid" && selectedBullet.type != "solid rn" && selectedBullet.type != "DGS" && selectedBullet.type != "Sledgehammer Solid" {
                            game.tagMF = .MF
                            possibleAnimals.append(game)
                        }
                }
                    if weightMale >= 110 && weightMale < 330 && weightFem >= 110 && weightFem < 330 && predator {
                        if selectedBullet.type != "fmj rn" && selectedBullet.type != "Barnes solid" && selectedBullet.type != "solid rn" && selectedBullet.type != "DGS" && selectedBullet.type != "Sledgehammer Solid" {
                            game.tagMF = .MF
                            possibleAnimals.append(game)
                        } // dodato zbog nove teorije
                    }
                    else if weightFem < 110 && weightMale >= 110 && weightMale < 330 && predator {
                        if selectedBullet.type != "fmj rn" && selectedBullet.type != "Barnes solid" && selectedBullet.type != "solid rn" && selectedBullet.type != "DGS" && selectedBullet.type != "Sledgehammer Solid" {
                            game.tagMF = .MF
                            possibleAnimals.append(game)
                        } // dodato zbog nove teorije
                    }
            }
        }
            
        } else if minChok >= 2800 {
            for game in specificGames {
                guard let predator = game.predator else { return [] }
                if let weight = game.weightMF {
                    if weight >= 1540 && weight < 3100 && !predator {
                            game.tagMF = .MF
                            possibleAnimals.append(game)
                    }
                    if weight >= 770 && weight < 1540 && predator {
                        if selectedBullet.type != "fmj rn" && selectedBullet.type != "Barnes solid" && selectedBullet.type != "solid rn" && selectedBullet.type != "DGS" && selectedBullet.type != "Sledgehammer Solid" {
                            game.tagMF = .MF
                            possibleAnimals.append(game)
                        }
                    }
                    if weight >= 330 && weight < 770 && predator {
                        if selectedBullet.type != "fmj rn" && selectedBullet.type != "Barnes solid" && selectedBullet.type != "solid rn" && selectedBullet.type != "DGS" && selectedBullet.type != "Sledgehammer Solid" {
                            game.tagMF = .MF
                            possibleAnimals.append(game)
                        }
                    }
                }
                if let weightMale = game.weightM, let weightFem = game.weightF {
                    if weightMale >= 770 && weightMale < 1540 && weightFem >= 770 && weightFem < 1540 && predator {
                        if selectedBullet.type != "fmj rn" && selectedBullet.type != "Barnes solid" && selectedBullet.type != "solid rn" && selectedBullet.type != "DGS" && selectedBullet.type != "Sledgehammer Solid" {
                            game.tagMF = .MF
                            possibleAnimals.append(game)
                        }
                    }
                    else if weightFem < 770 && weightMale >= 770 && weightMale < 1540 && predator {
                        if selectedBullet.type != "fmj rn" && selectedBullet.type != "Barnes solid" && selectedBullet.type != "solid rn" && selectedBullet.type != "DGS" && selectedBullet.type != "Sledgehammer Solid" {
                            game.tagMF = .MF
                            possibleAnimals.append(game)
                        }
                    }
                    if weightMale >= 330 && weightMale < 770 && weightFem >= 330 && weightFem < 770 && predator {
                        if selectedBullet.type != "fmj rn" && selectedBullet.type != "Barnes solid" && selectedBullet.type != "solid rn" && selectedBullet.type != "DGS" && selectedBullet.type != "Sledgehammer Solid" {
                            game.tagMF = .MF
                            possibleAnimals.append(game)
                        }
                    } // dodatyo zbog nove teorije
                    else if weightFem < 330 && weightMale >= 330 && weightMale < 770 && predator {
                        if selectedBullet.type != "fmj rn" && selectedBullet.type != "Barnes solid" && selectedBullet.type != "solid rn" && selectedBullet.type != "DGS" && selectedBullet.type != "Sledgehammer Solid" {
                            game.tagMF = .MF
                            possibleAnimals.append(game)
                        }
                    } // dodato zbog nove teorije
                    if weightMale >= 1540 && weightMale < 3100 && weightFem >= 1540 && weightFem < 3100 && !predator {
                            game.tagMF = .MF
                            possibleAnimals.append(game)
                    }
                    else if weightMale >= 1540 && weightMale < 3100 && weightFem < 1540 && !predator {
                            game.tagMF = .MF
                            possibleAnimals.append(game)
                    }
                }
            }
        }
       return possibleAnimals
    }
}


extension PossibleGamesController {
    
    func setupKeyboardNotificationHandlers() {
        let nc = NotificationCenter.default
        
        nc.addObserver(forName: UIResponder.keyboardWillShowNotification, object: nil, queue: .main, using: keyboardWillShowNotificationHandler)
        
        nc.addObserver(forName: UIResponder.keyboardDidHideNotification, object: nil, queue: .main, using: keyboardDidHideNotificationHandler)
        
    }
    @objc func keyboardWillShowNotificationHandler(_ notification: Notification) {
        
        if let userInfo = notification.userInfo {
            if let frameValue = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
                let frame = frameValue.cgRectValue
                let keyboardVisibleHeight = frame.size.height
                
                self.tableView.contentInset.bottom = keyboardVisibleHeight
            }
        }
    }
    @objc func keyboardDidHideNotificationHandler(_ notification: Notification) {
        self.tableView.contentInset.bottom = 0
    }
}


extension PossibleGamesController: UITableViewDataSource {
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if searchActive {
            return animalsFiltered.count
        } else {
            return allAnimalsForBullet.count
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: GameCell = tableView.dequeueReusableCell(withIdentifier: GameCell.reuseIdentifier, for: indexPath) as! GameCell
        
        if searchActive && animalsFiltered.count > 0 {
            
            let sortedFilteredGames = animalsFiltered.sorted(by: { $0.name < $1.name })
            let finalSorted = sortedFilteredGames.sorted(by: { $0.tagMF.rawValue > $1.tagMF.rawValue })
            let cc = finalSorted[indexPath.row]
            if cc.tagMF == .MF {
                cell.setBlankAccessoryView()
            } else if cc.tagMF == .M {
                cell.setBlankAccessoryView()
            } else if cc.tagMF == .F {
                cell.setLetterFtoAccessoryView()
            }
            cell.configure(with: cc)

        } else {
            let sortedGames = allAnimalsForBullet.sorted(by: { $0.name < $1.name })
            let finalSorted = sortedGames.sorted(by: { $0.tagMF.rawValue > $1.tagMF.rawValue })
            let cc = finalSorted[indexPath.row]
            print(cc.tagMF.rawValue)
            if cc.tagMF == .MF {
                cell.setBlankAccessoryView()
            } else if cc.tagMF == .M {
                cell.setBlankAccessoryView()
            } else if cc.tagMF == .F {
                cell.setLetterFtoAccessoryView()
            }
            cell.configure(with: cc)
        }
        return cell
    }
}


extension PossibleGamesController: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true
        searchBar.setShowsCancelButton(true, animated: true)
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false
        searchBar.setShowsCancelButton(false, animated: true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
        searchBar.resignFirstResponder()
        searchBar.text = nil
        searchBar.setShowsCancelButton(false, animated: true)
        tableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        searchActive = false
        searchBar.resignFirstResponder()
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        animalsFiltered = []
        let searchT = searchText.lowercased()
        
        for game in allAnimalsForBullet {
            
            let gameName = game.name.lowercased()
            
            if gameName.contains(searchT) {
                animalsFiltered.append(game)
            } else { continue }
            
        }
        
        if (animalsFiltered.count == allAnimalsForBullet.count) || searchT.count == 0 {
            searchActive = false
        } else {
            searchActive = true
        }
        tableView.reloadData()
    }
}


extension PossibleGamesController {
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 2
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let vw = UIView(frame: CGRect(x:0, y:0, width:tableView.frame.size.width, height:18))
        let lineFrame = CGRect(x: 16, y: 0, width: (tableView.bounds.size.width - 32), height: 1)
        let line = UIView(frame: lineFrame)
        line.backgroundColor = UIColor.orange
        vw.addSubview(line)
        
        //vw.backgroundColor = UIColor.white
        if #available(iOS 13.0, *) {
                    vw.backgroundColor = UIColor.systemBackground
                } else {
                    vw.backgroundColor = UIColor.white
                }
        return vw
        
    }
}
