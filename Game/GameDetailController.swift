//
//  GameDetailController.swift
//  TrueBullet
//
//  Created by Milica Cokic on 5/1/18.
//  Copyright © 2018 Milica Cokic. All rights reserved.
//

import UIKit

final class GameDetailController: UIViewController {
    
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var upperView: UIView!
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var avWeightLabel: UILabel!
    @IBOutlet private weak var avWeightLabel2: UILabel!
    @IBOutlet private weak var maleWeight: UILabel!
    @IBOutlet private weak var femaleWeight: UILabel!
    @IBOutlet private weak var chokLabel: UILabel!
    @IBOutlet private weak var segmentedControl: UISegmentedControl!
    
    @IBOutlet private weak var constraintUp: NSLayoutConstraint?
    @IBOutlet private weak var constraintDown: NSLayoutConstraint?
    @IBOutlet private weak var segmentedHeight: NSLayoutConstraint?
    @IBOutlet private weak var distanceMaleWeight: NSLayoutConstraint?
    @IBOutlet private weak var equalEnds: NSLayoutConstraint?
    
    
    lazy var searchBar = UISearchBar(frame: CGRect.zero)
    private var searchActive: Bool = false
    
    private var myBulletStorage = BulletStorage.shared
    private var filteredBullets: [Bullet] = []
    private var bulletsForFiltering: [Bullet] = []
    
    private var index: Int = 0
    //private var isArrayArrived = false
    var game: GameSpecific?
     
    private var allBullets: [Bullet] = [] {
        // da li npr ovde treba provera isViewLoaded, menja se niz, a onda reloaduje tabelu, sta ako view uopste nije ucitan? mada tableView property je weak, ako je nil, samo se nece nista desiti
        didSet {
            if index == 0 {
               bulletsForFiltering = filterByUsual(selectedGame: game)
            } else {
               bulletsForFiltering = filterByPossible(selectedGame: game)
            }
            tableView.reloadData()
            //isArrayArrived = true
        }
    }
   

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupKeyboardNotificationHandlers()
        setSearchBar()
        configureData()
        setSegmentedControl()
        
//        if let topItem = self.navigationController?.navigationBar.topItem {
//            topItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
//        }
        
        //tableView.tableFooterView = UIView()
        //tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        
        myBulletStorage.bulletsAll {
            [weak self] all in
            DispatchQueue.main.async {
                self?.allBullets = all
            }
        }
        
        settingColors()
    }
    

    override func viewWillAppear(_ animated: Bool) {
        
        if let index = self.tableView.indexPathForSelectedRow {
            self.tableView.deselectRow(at: index, animated: true)
        }
        if game?.weightMF != nil {
            constraintUp?.constant = 0
        }
    }
    
    
    private func settingColors() {
              
             if #available(iOS 13.0, *) {
                 upperView.backgroundColor = UIColor.systemBackground
             } else {
                 upperView.backgroundColor = UIColor.white
             }
              
              if #available(iOS 13.0, *) {
                  view.backgroundColor = UIColor.systemBackground
              } else {
                  view.backgroundColor = UIColor.white
              }
        }
       
    
    private func setSegmentedControl() {
        if self.view.traitCollection.horizontalSizeClass == UIUserInterfaceSizeClass.regular {
            segmentedHeight?.constant = 35
            let basicFont = UIFont.systemFont(ofSize: 18)
            let font = UIFont(name: "Avenir", size: 18.0)
            segmentedControl.setTitleTextAttributes([NSAttributedString.Key.font: font ?? basicFont], for: .normal)
        } else {
            segmentedHeight?.constant = 28
            let basicFont = UIFont.systemFont(ofSize: 13)
            let font = UIFont(name: "Avenir", size: 13.0)
            segmentedControl.setTitleTextAttributes([NSAttributedString.Key.font: font ?? basicFont], for: .normal)
        }
    }
    
    private func configureData() {
        
        nameLabel.text = game?.name
        if let weightMF = game?.weightMF {
            avWeightLabel2.isHidden = true
            avWeightLabel2.heightAnchor.constraint(equalToConstant: 0).isActive = true
            avWeightLabel.text = "Average Weight:"
            let avWInt = Int(weightMF)
            let avW = String(describing: avWInt)
            maleWeight.text = "\(avW) lbs"
            femaleWeight.isHidden = true
            femaleWeight.heightAnchor.constraint(equalToConstant: 0).isActive = true
            equalEnds?.isActive = false
        }
        if let weightM = game?.weightM, let weightF = game?.weightF {
            let wM = Int(weightM)
            let wF = Int(weightF)
            let weightMale = String(describing: wM)
            let weightFem = String(describing: wF)
            maleWeight.text = "\(weightMale) lbs"
            femaleWeight.text = "\(weightFem) lbs"
        }
        
    }
    
    private func setSearchBar() {
        searchBar.delegate = self
        searchBar.returnKeyType = UIReturnKeyType.done
        searchBar.placeholder = "Search"
        navigationItem.titleView = searchBar
    }
    
    
    @IBAction func didTapTCButton(_ sender: UIButton) {
        showTerms()
    }
    
    private func showTerms() {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        guard let vc = storyboard.instantiateViewController(withIdentifier: "TermsViewController") as? TermsViewController else {
            fatalError("could not find TermsViewController")
        }
        show(vc, sender: self)
    }
    
    
    @IBAction func indexChanged(_ sender: UISegmentedControl) {
        
        searchActive = false
        searchBar.resignFirstResponder()
        searchBar.text = nil
        searchBar.setShowsCancelButton(false, animated: true)
        
        switch segmentedControl.selectedSegmentIndex
        {
        case 0:
           bulletsForFiltering = filterByUsual(selectedGame: game)
           tableView.reloadData()
        case 1:
           bulletsForFiltering = filterByPossible(selectedGame: game)
           tableView.reloadData()
        default:
            break
        }
    }
}


extension GameDetailController {
    
    func setupKeyboardNotificationHandlers() {
        let nc = NotificationCenter.default
        
        nc.addObserver(forName: UIResponder.keyboardWillShowNotification, object: nil, queue: .main, using: keyboardWillShowNotificationHandler)
        
        nc.addObserver(forName: UIResponder.keyboardDidHideNotification, object: nil, queue: .main, using: keyboardDidHideNotificationHandler)
        
    }
    
    @objc func keyboardWillShowNotificationHandler(_ notification: Notification) {
        
        if let userInfo = notification.userInfo {
            if let frameValue = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
                let frame = frameValue.cgRectValue
                let keyboardVisibleHeight = frame.size.height
                
                self.tableView.contentInset.bottom = keyboardVisibleHeight
            }
        }
    }
    
    @objc func keyboardDidHideNotificationHandler(_ notification: Notification) {
        self.tableView.contentInset.bottom = 0
    }
}


extension GameDetailController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if searchActive {
            return filteredBullets.count
        } else {
            return bulletsForFiltering.count
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: BulletCell = tableView.dequeueReusableCell(withIdentifier: BulletCell.reuseIdentifier, for: indexPath) as! BulletCell
        
        if searchActive && filteredBullets.count > 0 {
                let sortedFilteredBullets = filteredBullets.sorted(by: { $0.name < $1.name })
                let finalSorted1 = sortedFilteredBullets.sorted(by: { $0.radius < $1.radius })
                let finalSorted = finalSorted1.sorted(by: { $0.orientation.rawValue > $1.orientation.rawValue })
                let cc = finalSorted[indexPath.row]
                if cc.orientation == .MF {
                    cell.setBlankAccessoryView()
                } else if cc.orientation == .M {
                    cell.setBlankAccessoryView()
                } else if cc.orientation == .F {
                    cell.setLetterFtoAccessoryView()
                }
                cell.configure(with: cc)
        } else {
            let sortedBullets = bulletsForFiltering.sorted(by: { $0.name < $1.name})
            let finalSorted1 = sortedBullets.sorted(by: { $0.radius < $1.radius })
            let finalSorted = finalSorted1.sorted(by: { $0.orientation.rawValue > $1.orientation.rawValue })
            
            let cc = finalSorted[indexPath.row]
            if cc.orientation == .MF {
                cell.setBlankAccessoryView()
            } else if cc.orientation == .M {
                cell.setBlankAccessoryView()
            } else if cc.orientation == .F {
                cell.setLetterFtoAccessoryView()
            }
              cell.configure(with: cc)
        }
        return cell
    }
}


extension GameDetailController {
    
    func filterByUsual(selectedGame: GameSpecific?) -> [Bullet] {

        for bullet in allBullets {
            bullet.orientation = .MF
        }
        
        var usualBullets: [Bullet] = []
        guard let game = selectedGame else { return [] }
        guard let predator = game.predator else { return [] }
        
        
        if let weight = game.weightMF {
            if weight > 0.5 && weight < 10 {
                for bullet in allBullets {
                    if bullet.chok0 >= 7 && bullet.chok0 < 30 {
                        bullet.orientation = .MF
                        usualBullets.append(bullet)
                    }
                }
            } else if weight >= 10 && weight < 40 {
                for bullet in allBullets {
                    if bullet.chok0 >= 30 && bullet.chok0 < 88 {
                        bullet.orientation = .MF
                        usualBullets.append(bullet)
                    }
                }
            } else if weight >= 40 && weight < 110 {
                for bullet in allBullets {
                    if bullet.chok0 >= 88 && bullet.chok0 < 270 {
                        bullet.orientation = .MF
                        usualBullets.append(bullet)
                    }
                }
         }
            else if weight >= 110 && weight < 330 && !predator {
                for bullet in allBullets {
                    if bullet.chok0 >= 270 && bullet.chok0 < 600 {
                        bullet.orientation = .MF
                        usualBullets.append(bullet)
                    }
                }
            } else if weight >= 110 && weight < 330 && predator {
                for bullet in allBullets {
                    if bullet.chok0 >= 600 && bullet.chok0 < 1000 && bullet.type != "fmj rn" && bullet.type != "Barnes solid" && bullet.type != "solid rn" && bullet.type != "DGS" && bullet.type != "Sledgehammer Solid" {
                        bullet.orientation = .MF
                        usualBullets.append(bullet)
                    }
                }
            } else if weight >= 330 && weight < 770 && !predator {
                for bullet in allBullets {
                    if bullet.chok0 >= 600 && bullet.chok0 < 1000 {
                        bullet.orientation = .MF
                        usualBullets.append(bullet)
                    }
                }
            } else if weight >= 330 && weight < 770 && predator {
                for bullet in allBullets {
                    if bullet.chok0 >= 1000 && bullet.chok0 < 1650 && bullet.type != "fmj rn" && bullet.type != "Barnes solid" && bullet.type != "solid rn" && bullet.type != "DGS" && bullet.type != "Sledgehammer Solid" {
                        bullet.orientation = .MF
                        usualBullets.append(bullet)
                    }
                }
            } else if weight >= 770 && weight < 1540 && !predator {
                for bullet in allBullets {
                    if bullet.chok0 >= 1000 && bullet.chok0 < 1650 {
                        bullet.orientation = .MF
                        usualBullets.append(bullet)
                    }
                }
            } else if weight >= 770 && weight < 1540 && predator {
                for bullet in allBullets {
                    if bullet.chok0 >= 1650 && bullet.chok0 < 2800 && bullet.type != "fmj rn" && bullet.type != "Barnes solid" && bullet.type != "solid rn" && bullet.type != "DGS" && bullet.type != "Sledgehammer Solid" {
                        bullet.orientation = .MF
                        usualBullets.append(bullet)
                    }
                }
            } else if weight >= 1540 && weight < 3100 {
                for bullet in allBullets {
                    if bullet.chok0 >= 1650 && bullet.chok0 < 2800 {
                        bullet.orientation = .MF
                        usualBullets.append(bullet)
                    }
                }
             }
            else if weight >= 3100 {
                for bullet in allBullets {
                    if bullet.chok0 >= 2800 {
                        bullet.orientation = .MF
                        usualBullets.append(bullet)
                    }
                }
            }
        } else if let weightM = game.weightM, let weightF = game.weightF {
            if weightM > 0.5 && weightM < 10 && weightF > 0.5 && weightF < 10 {
                for bullet in allBullets {
                    if bullet.chok0 >= 7 && bullet.chok0 < 30 {
                        bullet.orientation = .MF
                        usualBullets.append(bullet)
                    }
                }
            } else if weightF >= 10 && weightF < 40 && weightM >= 10 && weightM < 40 {
                for bullet in allBullets {
                    if bullet.chok0 >= 30 && bullet.chok0 < 88 {
                        bullet.orientation = .MF
                        usualBullets.append(bullet)
                    }
                }
            } else if weightF >= 10 && weightF < 40 && weightM >= 40 && !predator {
                for bullet in allBullets {
                    if bullet.chok0 >= 88 && bullet.chok0 < 270 {
                        bullet.orientation = .MF
                        usualBullets.append(bullet)
                    }
                }
            } else if weightF > 0.5 && weightF < 10 && weightM >= 10 && weightM < 40 {
                for bullet in allBullets {
                    if bullet.chok0 >= 30 && bullet.chok0 < 88 {
                        bullet.orientation = .MF
                        usualBullets.append(bullet)
                    }
                }
            } else if weightF >= 40 && weightF < 110 && weightM >= 40 && weightM < 110 && !predator {
                for bullet in allBullets {
                    if bullet.chok0 >= 88 && bullet.chok0 < 270 {
                        bullet.orientation = .MF
                        usualBullets.append(bullet)
                    }
                }
            } else if weightF >= 40 && weightF < 110 && weightM >= 110 && !predator {
                for bullet in allBullets {
                    if bullet.chok0 >= 270 && bullet.chok0 < 600 {
                        bullet.orientation = .MF
                        usualBullets.append(bullet)
                    }
                }
            } else if weightF < 40 && weightM >= 40 && weightM < 110 && !predator {
                for bullet in allBullets {
                    if bullet.chok0 >= 88 && bullet.chok0 < 270 {
                        bullet.orientation = .MF
                        usualBullets.append(bullet)
                    }
                }
            }
            else if weightF >= 40 && weightF < 110 && weightM >= 110 && predator {
                for bullet in allBullets {
                    if bullet.chok0 >= 600 && bullet.chok0 < 1000 && bullet.type != "fmj rn" && bullet.type != "Barnes solid" && bullet.type != "solid rn" && bullet.type != "DGS" && bullet.type != "Sledgehammer Solid" {
                        bullet.orientation = .MF
                        usualBullets.append(bullet)
                    }
                }
            }
            else if weightF >= 110 && weightF < 330 && weightM >= 110 && weightM < 330 && !predator {
                for bullet in allBullets {
                    if bullet.chok0 >= 270 && bullet.chok0 < 600 {
                        bullet.orientation = .MF
                        usualBullets.append(bullet)
                    }
                }
            } else if weightF >= 110 && weightF < 330 && weightM >= 330 && !predator {
                for bullet in allBullets {
                    if bullet.chok0 >= 600 && bullet.chok0 < 1000 {
                        bullet.orientation = .MF
                        usualBullets.append(bullet)
                    }
                }
            } else if weightF < 110 && weightM >= 110 && weightM < 330 && !predator {
                for bullet in allBullets {
                    if bullet.chok0 >= 270 && bullet.chok0 < 600 {
                        bullet.orientation = .MF
                        usualBullets.append(bullet)
                    }
                }
            } else if weightF >= 110 && weightF < 330 && weightM >= 110 && weightM < 330 && predator {
                for bullet in allBullets {
                    if bullet.chok0 >= 600 && bullet.chok0 < 1000 && bullet.type != "fmj rn" && bullet.type != "Barnes solid" && bullet.type != "solid rn" && bullet.type != "DGS" && bullet.type != "Sledgehammer Solid" {
                        bullet.orientation = .MF
                        usualBullets.append(bullet)
                    }
                }
            } else if weightF >= 110 && weightF < 330 && weightM >= 330 && predator {
                for bullet in allBullets {
                    if bullet.chok0 >= 1000 && bullet.chok0 < 1650 && bullet.type != "fmj rn" && bullet.type != "Barnes solid" && bullet.type != "solid rn" && bullet.type != "DGS" && bullet.type != "Sledgehammer Solid" {
                        bullet.orientation = .MF
                        usualBullets.append(bullet)
                    }
                }
            } else if weightF < 110 && weightM >= 110 && weightM < 330 && predator {
                for bullet in allBullets {
                    if bullet.chok0 >= 600 && bullet.chok0 < 1000 && bullet.type != "fmj rn" && bullet.type != "Barnes solid" && bullet.type != "solid rn" && bullet.type != "DGS" && bullet.type != "Sledgehammer Solid" {
                        bullet.orientation = .MF
                        usualBullets.append(bullet)
                    }
                }
            } else if weightF >= 330 && weightF < 770 && weightM >= 330 && weightM < 770 && !predator {
                for bullet in allBullets {
                    if bullet.chok0 >= 600 && bullet.chok0 < 1000 {
                        bullet.orientation = .MF
                        usualBullets.append(bullet)
                    }
                }
            } else if weightF >= 330 && weightF < 770 && weightM >= 770 && !predator {
                for bullet in allBullets {
                    if bullet.chok0 >= 1000 && bullet.chok0 < 1650 {
                        bullet.orientation = .MF
                        usualBullets.append(bullet)
                    }
                }
            } else if weightF < 330 && weightM >= 330 && weightM < 770 && !predator {
                for bullet in allBullets {
                    if bullet.chok0 >= 600 && bullet.chok0 < 1000 {
                        bullet.orientation = .MF
                        usualBullets.append(bullet)
                    }
                }
            } else if weightF >= 330 && weightF < 770 && weightM >= 330 && weightM < 770 && predator {
                for bullet in allBullets {
                    if bullet.chok0 >= 1000 && bullet.chok0 < 1650 && bullet.type != "fmj rn" && bullet.type != "Barnes solid" && bullet.type != "solid rn" && bullet.type != "DGS" && bullet.type != "Sledgehammer Solid" {
                        bullet.orientation = .MF
                        usualBullets.append(bullet)
                    }
                }
            } else if weightF >= 330 && weightF < 770 && weightM >= 770 && predator {
                for bullet in allBullets {
                    if bullet.chok0 >= 1650 && bullet.chok0 < 2800 && bullet.type != "fmj rn" && bullet.type != "Barnes solid" && bullet.type != "solid rn" && bullet.type != "DGS" && bullet.type != "Sledgehammer Solid" {
                        bullet.orientation = .MF
                        usualBullets.append(bullet)
                    }
                }
            } else if weightF < 330 && weightM >= 330 && weightM < 770 && predator {
                for bullet in allBullets {
                    if bullet.chok0 >= 1000 && bullet.chok0 < 1650 && bullet.type != "fmj rn" && bullet.type != "Barnes solid" && bullet.type != "solid rn" && bullet.type != "DGS" && bullet.type != "Sledgehammer Solid" {
                        bullet.orientation = .MF
                        usualBullets.append(bullet)
                    }
                }
            } else if weightF >= 770 && weightF < 1540 && weightM >= 770 && weightM < 1540 && !predator {
                for bullet in allBullets {
                    if bullet.chok0 >= 1000 && bullet.chok0 < 1650 {
                        bullet.orientation = .MF
                        usualBullets.append(bullet)
                    }
                }
            } else if weightF >= 770 && weightF < 1540 && weightM >= 1540 && !predator {
                for bullet in allBullets {
                    if bullet.chok0 >= 1650 && bullet.chok0 < 2800 {
                        bullet.orientation = .MF
                        usualBullets.append(bullet)
                    }
                }
            } else if weightF < 770 && weightM >= 770 && weightM < 1540 && !predator {
                for bullet in allBullets {
                    if bullet.chok0 >= 1000 && bullet.chok0 < 1650 {
                        bullet.orientation = .MF
                        usualBullets.append(bullet)
                    }
                }
            } else if weightF >= 770 && weightF < 1540 && weightM >= 770 && weightM < 1540 && predator {
                for bullet in allBullets {
                    if bullet.chok0 >= 1650 && bullet.chok0 < 2800 && bullet.type != "fmj rn" && bullet.type != "Barnes solid" && bullet.type != "solid rn" && bullet.type != "DGS" && bullet.type != "Sledgehammer Solid" {
                        bullet.orientation = .MF
                        usualBullets.append(bullet)
                    }
                }
            } else if weightF < 770 && weightM >= 770 && weightM < 1540 && predator {
                for bullet in allBullets {
                    if bullet.chok0 >= 1650 && bullet.chok0 < 2800 && bullet.type != "fmj rn" && bullet.type != "Barnes solid" && bullet.type != "solid rn" && bullet.type != "DGS" && bullet.type != "Sledgehammer Solid" {
                        bullet.orientation = .MF
                        usualBullets.append(bullet)
                    }
                }
            } else if weightF >= 1540 && weightF < 3100 && weightM >= 1540 && weightM < 3100 {
                for bullet in allBullets {
                    if bullet.chok0 >= 1650 && bullet.chok0 < 2800 {
                        bullet.orientation = .MF
                        usualBullets.append(bullet)
                    }
                }
            } else if weightF >= 1540 && weightF < 3100 && weightM >= 3100 {
                for bullet in allBullets {
                    if bullet.chok0 >= 2800 {
                        bullet.orientation = .MF
                        usualBullets.append(bullet)
                    }
                }
            } else if weightF < 1540 && weightM >= 1540 && weightM < 3100 {
                for bullet in allBullets {
                    if bullet.chok0 >= 1650 && bullet.chok0 < 2800 {
                        bullet.orientation = .MF
                        usualBullets.append(bullet)
                    }
                }
            } else if weightF >= 3100 && weightM >= 3100 {
                for bullet in allBullets {
                    if bullet.chok0 >= 2800 {
                        bullet.orientation = .MF
                        usualBullets.append(bullet)
                    }
                }
            } else if weightF < 3100 && weightM >= 3100 {
                for bullet in allBullets {
                    if bullet.chok0 >= 2800 {
                        bullet.orientation = .MF
                        usualBullets.append(bullet)
                    }
                }
            }
        }
        
        return usualBullets
    }
    
    
    func filterByPossible(selectedGame: GameSpecific?) -> [Bullet] {
        
        for bullet in allBullets {
            bullet.orientation = .MF
        }
        
        var possibleBullets: [Bullet] = []
        guard let game = selectedGame else { return [] }
        guard let predator = game.predator else { return [] }
        
        if let weight = game.weightMF {
            if weight > 0.5 && weight < 10 {
                for bullet in allBullets {
                    if bullet.chok0 >= 30 && bullet.chok0 < 88 {
                        bullet.orientation = .MF
                        possibleBullets.append(bullet)
                    }
                }
            }
            else if weight >= 10 && weight < 40 {
                for bullet in allBullets {
                    if bullet.chok0 >= 88 && bullet.chok0 < 270 {
                        bullet.orientation = .MF
                        possibleBullets.append(bullet)
                    }
                }
            }
            else if weight >= 40 && weight < 110 {
                for bullet in allBullets {
                    if bullet.chok0 >= 270 && bullet.chok0 < 600 {
                        bullet.orientation = .MF
                        possibleBullets.append(bullet)
                    }
                }
            }
            else if weight >= 110 && weight < 330 && !predator {
                for bullet in allBullets {
                    if bullet.chok0 >= 600 && bullet.chok0 < 1000 {
                        bullet.orientation = .MF
                        possibleBullets.append(bullet)
                    }
                }
            } else if weight >= 110 && weight < 330 && predator {
                for bullet in allBullets {
                    if bullet.chok0 >= 1000 && bullet.chok0 < 2800 && bullet.type != "fmj rn" && bullet.type != "Barnes solid" && bullet.type != "solid rn" && bullet.type != "DGS" && bullet.type != "Sledgehammer Solid" {
                        bullet.orientation = .MF
                        possibleBullets.append(bullet)
                    }
                }
            } else if weight >= 330 && weight < 770 && !predator {
                for bullet in allBullets {
                    if bullet.chok0 >= 1000 && bullet.chok0 < 1650 {
                        bullet.orientation = .MF
                        possibleBullets.append(bullet)
                    }
                }
            } else if weight >= 330 && weight < 770 && predator {
                for bullet in allBullets {
                    if bullet.chok0 >= 1650 && bullet.type != "fmj rn" && bullet.type != "Barnes solid" && bullet.type != "solid rn" && bullet.type != "DGS" && bullet.type != "Sledgehammer Solid" {
                        bullet.orientation = .MF
                        possibleBullets.append(bullet)
                    }
                }
            } else if weight >= 770 && weight < 1540 && !predator {
                for bullet in allBullets {
                    if bullet.chok0 >= 1650 && bullet.chok0 < 2800 {
                        bullet.orientation = .MF
                        possibleBullets.append(bullet)
                    }
                }
            } else if weight >= 770 && weight < 1540 && predator {
                for bullet in allBullets {
                    if bullet.chok0 >= 2800 && bullet.type != "fmj rn" && bullet.type != "Barnes solid" && bullet.type != "solid rn" && bullet.type != "DGS" && bullet.type != "Sledgehammer Solid" {
                        bullet.orientation = .MF
                        possibleBullets.append(bullet)
                    }
                }
            } else if weight >= 1540 && weight < 3100 {
                for bullet in allBullets {
                    if bullet.type == "fmj rn" || bullet.type == "Barnes solid" || bullet.type == "solid rn" || bullet.type == "DGS" || bullet.type == "Sledgehammer Solid" {
                        if bullet.chok0 >= 1000 && bullet.chok0 < 1650 {
                            bullet.orientation = .MF
                            possibleBullets.append(bullet)
                        }
                    }
                    if bullet.chok0 >= 2800 {
                        bullet.orientation = .MF
                        possibleBullets.append(bullet)
                    }
                }
            } else if weight >= 3100 {
                for bullet in allBullets {
                    if bullet.type == "fmj rn" || bullet.type == "Barnes solid" || bullet.type == "solid rn" || bullet.type == "DGS" || bullet.type == "Sledgehammer Solid" {
                        if bullet.chok0 >= 1650 && bullet.chok0 < 2800 {
                            bullet.orientation = .MF
                            possibleBullets.append(bullet)
                        }
                    }
                }
            }
        } else if let weightM = game.weightM, let weightF = game.weightF {
            if weightM > 0.5 && weightM < 10 && weightF > 0.5 && weightF < 10 {
                for bullet in allBullets {
                    if bullet.chok0 >= 30 && bullet.chok0 < 88 {
                        bullet.orientation = .MF
                        possibleBullets.append(bullet)
                    }
                }
            } else if weightF > 0.5 && weightF < 10 && weightM >= 10 {
                for bullet in allBullets {
                    if bullet.chok0 >= 7 && bullet.chok0 < 30 {
                            bullet.orientation = .F
                            possibleBullets.append(bullet)
                        } // prebaceno iz usual
                    else if bullet.chok0 >= 88 && bullet.chok0 < 270 {
                        bullet.orientation = .MF
                        possibleBullets.append(bullet)
                    }
                }
            }
            else if weightF >= 10 && weightF < 40 && weightM >= 10 && weightM < 40 {
                for bullet in allBullets {
                    if bullet.chok0 >= 88 && bullet.chok0 < 270 {
                        bullet.orientation = .MF
                        possibleBullets.append(bullet)
                    }
                }
                
            } else if weightF >= 10 && weightF < 40 && weightM >= 40 {
                for bullet in allBullets {
                    if bullet.chok0 >= 270 && bullet.chok0 < 600 {
                        bullet.orientation = .MF
                        possibleBullets.append(bullet)
                    }
                    else if bullet.chok0 >= 30 && bullet.chok0 < 88 {
                        bullet.orientation = .F
                        possibleBullets.append(bullet)
                    }
                }
            }
            else if weightF > 0.5 && weightF < 10 && weightM >= 10 && weightM < 40 {
                for bullet in allBullets {
                    if bullet.chok0 >= 88 && bullet.chok0 < 270 {
                        bullet.orientation = .MF
                        possibleBullets.append(bullet)
                    }
                    if bullet.chok0 >= 7 && bullet.chok0 < 30 {
                        bullet.orientation = .F
                        possibleBullets.append(bullet)
                    } // prebaceno iz usual
                }
            }
            else if weightF >= 40 && weightF < 110 && weightM >= 40 && weightM < 110 {
                for bullet in allBullets {
                    if bullet.chok0 >= 270 && bullet.chok0 < 600 {
                        bullet.orientation = .MF
                        possibleBullets.append(bullet)
                    }
                }
            } else if weightF >= 40 && weightF < 110 && weightM >= 110 && !predator {
                for bullet in allBullets {
                    if bullet.chok0 >= 600 && bullet.chok0 < 1000 {
                        bullet.orientation = .MF
                        possibleBullets.append(bullet)
                    }
                    if bullet.chok0 >= 88 && bullet.chok0 < 270 {
                        bullet.orientation = .F
                        possibleBullets.append(bullet)
                    } // prebaceno iz usual
                }
            }
            else if weightF < 40 && weightM >= 40 && weightM < 110 {
                for bullet in allBullets {
                    if bullet.chok0 >= 270 && bullet.chok0 < 600 {
                        bullet.orientation = .MF
                        possibleBullets.append(bullet)
                    }
                    if bullet.chok0 >= 30 && bullet.chok0 < 88 {
                        bullet.orientation = .F
                        possibleBullets.append(bullet)
                    } // prebaceno iz usual
                }
            }
            else if weightF >= 40 && weightF < 110 && weightM >= 110 && predator {
                for bullet in allBullets {
                    if bullet.chok0 >= 1000 && bullet.chok0 < 2800 && bullet.type != "fmj rn" && bullet.type != "Barnes solid" && bullet.type != "solid rn" && bullet.type != "DGS" && bullet.type != "Sledgehammer Solid" {
                        bullet.orientation = .MF
                        possibleBullets.append(bullet)
                    }
                }
            }
            else if weightF >= 110 && weightF < 330 && weightM >= 110 && weightM < 330 && !predator {
                for bullet in allBullets {
                    if bullet.chok0 >= 600 && bullet.chok0 < 1000 {
                        bullet.orientation = .MF
                        possibleBullets.append(bullet)
                    }
                }
            } else if weightF >= 110 && weightF < 330 && weightM >= 330 && !predator {
                for bullet in allBullets {
                    if bullet.chok0 >= 1000 && bullet.chok0 < 1650 {
                        bullet.orientation = .MF
                        possibleBullets.append(bullet)
                    }
                    if bullet.chok0 >= 270 && bullet.chok0 < 600 {
                        bullet.orientation = .F
                        possibleBullets.append(bullet)
                    }
                }
            }
            else if weightF < 110 && weightM >= 110 && weightM < 330 && !predator {
                for bullet in allBullets {
                    if bullet.chok0 >= 600 && bullet.chok0 < 1000 {
                        bullet.orientation = .MF
                        possibleBullets.append(bullet)
                    }
                    if bullet.chok0 >= 88 && bullet.chok0 < 270 {
                        bullet.orientation = .F
                        possibleBullets.append(bullet)
                    }
                }
            }
            else if weightF >= 110 && weightF < 330 && weightM >= 110 && weightM < 330 && predator {
                for bullet in allBullets {
                    if bullet.chok0 >= 1000 && bullet.chok0 < 2800 && bullet.type != "fmj rn" && bullet.type != "Barnes solid" && bullet.type != "solid rn" && bullet.type != "DGS" && bullet.type != "Sledgehammer Solid" {
                        bullet.orientation = .MF
                        possibleBullets.append(bullet)
                    }
                }
            } else if weightF >= 110 && weightF < 330 && weightM >= 330 && predator {
                for bullet in allBullets {
                    if bullet.chok0 >= 1650 && bullet.type != "fmj rn" && bullet.type != "Barnes solid" && bullet.type != "solid rn" && bullet.type != "DGS" && bullet.type != "Sledgehammer Solid" {
                        bullet.orientation = .MF
                        possibleBullets.append(bullet)
                    }
                }
            }
            else if weightF >= 330 && weightF < 770 && weightM >= 330 && weightM < 770 && !predator {
                for bullet in allBullets {
                    if bullet.chok0 >= 1000 && bullet.chok0 < 1650 {
                        bullet.orientation = .MF
                        possibleBullets.append(bullet)
                    }
                }
            } else if weightF >= 330 && weightF < 770 && weightM >= 770 && !predator {
                for bullet in allBullets {
                    if bullet.chok0 >= 1650 && bullet.chok0 < 2800 {
                        bullet.orientation = .MF
                        possibleBullets.append(bullet)
                    }
                    if bullet.chok0 >= 600 && bullet.chok0 < 1000 {
                        bullet.orientation = .F
                        possibleBullets.append(bullet)
                    } // prebaceno iz usual
                }
            }
            else if weightF < 330 && weightM >= 330 && weightM < 770 && !predator {
                for bullet in allBullets {
                    if bullet.chok0 >= 1000 && bullet.chok0 < 1650 {
                        bullet.orientation = .MF
                        possibleBullets.append(bullet)
                    }
                    if bullet.chok0 >= 270 && bullet.chok0 < 600 {
                        bullet.orientation = .F
                        possibleBullets.append(bullet)
                    } // prebaceno iz usual
                }
            } else if weightF >= 330 && weightF < 770 && weightM >= 330 && weightM < 770 && predator {
                for bullet in allBullets {
                    if bullet.chok0 >= 1650 && bullet.type != "fmj rn" && bullet.type != "Barnes solid" && bullet.type != "solid rn" && bullet.type != "DGS" && bullet.type != "Sledgehammer Solid" {
                        bullet.orientation = .MF
                        possibleBullets.append(bullet)
                    }
                }
            } else if weightF >= 330 && weightF < 770 && weightM >= 770 && predator {
                for bullet in allBullets {
                    if bullet.chok0 >= 2800 && bullet.type != "fmj rn" && bullet.type != "Barnes solid" && bullet.type != "solid rn" && bullet.type != "DGS" && bullet.type != "Sledgehammer Solid" {
                        bullet.orientation = .MF
                        possibleBullets.append(bullet)
                    }
                }
                
            } else if weightF < 330 && weightM >= 330 && weightM < 770 && predator {
                for bullet in allBullets {
                    if bullet.chok0 >= 1650 && bullet.type != "fmj rn" && bullet.type != "Barnes solid" && bullet.type != "solid rn" && bullet.type != "DGS" && bullet.type != "Sledgehammer Solid" {
                        bullet.orientation = .MF
                        possibleBullets.append(bullet)
                    }
                }
            }
            else if weightF >= 770 && weightF < 1540 && weightM >= 770 && weightM < 1540 && !predator {
                for bullet in allBullets {
                    if bullet.chok0 >= 1650 && bullet.chok0 < 2800 {
                        bullet.orientation = .MF
                        possibleBullets.append(bullet)
                    }
                }
            }
            else if weightF >= 770 && weightF < 1540 && weightM >= 1540 && !predator {
                for bullet in allBullets {
                    if bullet.type == "fmj rn" || bullet.type == "Barnes solid" || bullet.type == "solid rn" || bullet.type == "DGS" || bullet.type == "Sledgehammer Solid" {
                        if bullet.chok0 >= 1000 && bullet.chok0 < 1650 {
                            bullet.orientation = .MF
                            possibleBullets.append(bullet)
                        }
                    }
                    if bullet.type != "fmj rn" && bullet.type != "Barnes solid" && bullet.type != "solid rn" && bullet.type != "DGS" && bullet.type != "Sledgehammer Solid" {
                        if bullet.chok0 >= 1000 && bullet.chok0 < 1650 {
                            bullet.orientation = .F
                            possibleBullets.append(bullet)
                        }
                    }
                    if bullet.chok0 >= 2800 {
                        bullet.orientation = .MF
                        possibleBullets.append(bullet)
                    }
                }
            }
            else if weightF < 770 && weightM >= 770 && weightM < 1540 && !predator {
                for bullet in allBullets {
                    if bullet.chok0 >= 1650 && bullet.chok0 < 2800 {
                        bullet.orientation = .MF
                        possibleBullets.append(bullet)
                    }
                    if bullet.chok0 >= 600 && bullet.chok0 < 1000 {
                        bullet.orientation = .F
                        possibleBullets.append(bullet)
                    }
                }
            } else if weightF >= 770 && weightF < 1540 && weightM >= 770 && weightM < 1540 && predator {
                for bullet in allBullets {
                    if bullet.chok0 >= 2800 && bullet.type != "fmj rn" && bullet.type != "Barnes solid" && bullet.type != "solid rn" && bullet.type != "DGS" && bullet.type != "Sledgehammer Solid" {
                        bullet.orientation = .MF
                        possibleBullets.append(bullet)
                    }
                }
            } else if weightF < 770 && weightM >= 770 && weightM < 1540 && predator {
                for bullet in allBullets {
                    if bullet.chok0 >= 2800 && bullet.type != "fmj rn" && bullet.type != "Barnes solid" && bullet.type != "solid rn" && bullet.type != "DGS" && bullet.type != "Sledgehammer Solid" {
                        bullet.orientation = .MF
                        possibleBullets.append(bullet)
                    }
                }
            } else if weightF >= 1540 && weightF < 3100 && weightM >= 1540 && weightM < 3100 && !predator {
                for bullet in allBullets {
                    if bullet.type == "fmj rn" || bullet.type == "Barnes solid" || bullet.type == "solid rn" || bullet.type == "DGS" || bullet.type == "Sledgehammer Solid" {
                        if bullet.chok0 >= 1000 && bullet.chok0 < 1650 {
                            bullet.orientation = .MF
                            possibleBullets.append(bullet)
                        }
                    }
                    if bullet.chok0 >= 2800 {
                        bullet.orientation = .MF
                        possibleBullets.append(bullet)
                    }
                }
            }
            else if weightF >= 1540 && weightF < 3100 && weightM >= 3100 && !predator {
                for bullet in allBullets {
                    if bullet.type == "fmj rn" || bullet.type == "Barnes solid" || bullet.type == "solid rn" || bullet.type == "DGS" || bullet.type == "Sledgehammer Solid" {
                        if bullet.chok0 >= 1650 && bullet.chok0 < 2800 {
                            bullet.orientation = .MF
                            possibleBullets.append(bullet)
                        }
                    }
                    if bullet.type != "fmj rn" && bullet.type != "Barnes solid" && bullet.type != "solid rn" && bullet.type != "DGS" && bullet.type != "Sledgehammer Solid" {
                        if bullet.chok0 >= 1650 && bullet.chok0 < 2800 {
                            bullet.orientation = .F
                            possibleBullets.append(bullet)
                        }
                    }
                }
            }
            //ovo donje proveriti jos jednom sa tatom
            else if weightF >= 770 && weightF < 1540 && weightM >= 1540 && !predator {
                for bullet in allBullets {
                    if bullet.type == "fmj rn" || bullet.type == "Barnes solid" || bullet.type == "solid rn" || bullet.type == "DGS" || bullet.type == "Sledgehammer Solid" {
                        if bullet.chok0 >= 1000 && bullet.chok0 < 1650 {
                            bullet.orientation = .MF
                            possibleBullets.append(bullet)
                        }
                    }
                    if bullet.type != "fmj rn" && bullet.type != "Barnes solid" && bullet.type != "solid rn" && bullet.type != "DGS" && bullet.type != "Sledgehammer Solid" {
                        if bullet.chok0 >= 1000 && bullet.chok0 < 1650 {
                            bullet.orientation = .F
                            possibleBullets.append(bullet)
                        }
                    }
                    if bullet.chok0 >= 2800 {
                        bullet.orientation = .MF
                        possibleBullets.append(bullet)
                    }
                }
            } // ovo gore, mislim da je isto sa ovim dole, ali nema veze
            else if weightF < 1540 && weightM >= 1540 && weightM < 3100 && !predator {
                for bullet in allBullets {
                    if bullet.type == "fmj rn" || bullet.type == "Barnes solid" || bullet.type == "solid rn" || bullet.type == "DGS" || bullet.type == "Sledgehammer Solid" {
                        if bullet.chok0 >= 1000 && bullet.chok0 < 1650 {
                            bullet.orientation = .MF
                            possibleBullets.append(bullet)
                        }
                    }
                    if bullet.type != "fmj rn" && bullet.type != "Barnes solid" && bullet.type != "solid rn" && bullet.type != "DGS" && bullet.type != "Sledgehammer Solid" {
                        if bullet.chok0 >= 1000 && bullet.chok0 < 1650 {
                            bullet.orientation = .F
                            possibleBullets.append(bullet)
                        }
                    }
                    if bullet.chok0 >= 2800 {
                        bullet.orientation = .MF
                        possibleBullets.append(bullet)
                    }
                }
            }
            else if weightF >= 3100 && weightM >= 3100 {
                for bullet in allBullets {
                    if bullet.type == "fmj rn" || bullet.type == "Barnes solid" || bullet.type == "solid rn" || bullet.type == "DGS" || bullet.type == "Sledgehammer Solid" {
                        if bullet.chok0 >= 1650 && bullet.chok0 < 2800 {
                            bullet.orientation = .MF
                            possibleBullets.append(bullet)
                        }
                    }
                }
            }
            else if weightF < 3100 && weightM >= 3100 {
                for bullet in allBullets {
                    if bullet.type == "fmj rn" || bullet.type == "Barnes solid" || bullet.type == "solid rn" || bullet.type == "DGS" || bullet.type == "Sledgehammer Solid" {
                        if bullet.chok0 >= 1650 && bullet.chok0 < 2800 {
                            bullet.orientation = .MF
                            possibleBullets.append(bullet)
                        }
                    }
                    if bullet.chok0 >= 1650 && bullet.chok0 < 2800 && bullet.type != "fmj rn" && bullet.type != "Barnes solid" && bullet.type != "solid rn" && bullet.type != "DGS" && bullet.type != "Sledgehammer Solid"  {
                        bullet.orientation = .F
                        possibleBullets.append(bullet)
                    }
                }
            }
        }
        return possibleBullets
    }
}


extension GameDetailController: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true
        searchBar.setShowsCancelButton(true, animated: true)
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false
        searchBar.setShowsCancelButton(false, animated: true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
        searchBar.resignFirstResponder()
        searchBar.text = nil
        searchBar.setShowsCancelButton(false, animated: true)
        tableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        searchActive = false
        searchBar.resignFirstResponder()
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        filteredBullets = []
        let searchT = searchText.lowercased()
        
        for bullet in bulletsForFiltering {
            
            var fullBulletID = ""
            var fullBulletIDTrim = ""
            var bulletM = ""
            var bulletMTrim = ""
            var slugWeight = ""
            var bulletW = ""
            var bulletWTrim = ""
            var weightForSearch = ""
            var weightForSearchTrim = ""
            
            if let weight = bullet.weight {
                bulletW = String(describing: weight).lowercased()
                bulletWTrim = bulletW.replacingOccurrences(of: " ", with: "")
                var shortWeight = 1
                if !(weight - floor(weight) > 0.000001) {
                    shortWeight = Int(weight)
                    weightForSearch = "\(String(describing: shortWeight)) gr"
                    weightForSearchTrim = weightForSearch.replacingOccurrences(of: " ", with: "")
                } else {
                    weightForSearch = "\(String(describing: weight)) gr"
                    weightForSearchTrim = weightForSearch.replacingOccurrences(of: " ", with: "")
                }
            }
            
            let bulletN = bullet.name.lowercased()
            let bulletNTrim = bulletN.replacingOccurrences(of: " ", with: "")
            let bulletT = bullet.type.lowercased()
            let bulletTTrim = bulletT.replacingOccurrences(of: " ", with: "")
            
            if let manufact = bullet.manufacturer {
                bulletM = manufact.lowercased()
                bulletMTrim = bulletM.replacingOccurrences(of: " ", with: "")
            }
            if let slug = bullet.slugWeight {
                slugWeight = slug.lowercased()
                fullBulletID = "\(bulletN) \(bulletM) \(slugWeight) \(bulletT)"
                fullBulletIDTrim = "\(bulletNTrim) \(bulletMTrim) \(slugWeight) \(bulletTTrim)"
            } else {
                fullBulletID = "\(bulletN) \(weightForSearch) \(bulletT) \(bulletM)"
                fullBulletIDTrim = "\(bulletNTrim) \(weightForSearchTrim) \(bulletTTrim) \(bulletMTrim)"
            }
            
            let searchTNoSpaces = searchT.replacingOccurrences(of: " ", with: "")
            
            if fullBulletID.contains(searchT) || bulletN.contains(searchT) || bulletM.contains(searchT) || bulletT.contains(searchT) || bulletW.contains(searchT) || slugWeight.contains(searchT) || fullBulletID.contains(searchTNoSpaces) || bulletN.contains(searchTNoSpaces) || bulletM.contains(searchTNoSpaces) || bulletT.contains(searchTNoSpaces) || bulletW.contains(searchTNoSpaces) || slugWeight.contains(searchTNoSpaces) || fullBulletIDTrim.contains(searchT) || bulletNTrim.contains(searchT) || bulletMTrim.contains(searchT) || bulletTTrim.contains(searchT) || bulletWTrim.contains(searchT) || slugWeight.contains(searchT)
            {
                filteredBullets.append(bullet)
            }
            else { continue }
            
            //let bulletW = String(describing: bullet.weight).lowercased()
//            let bulletN = bullet.name.lowercased()
//            if let manufact = bullet.manufacturer {
//                bulletM = manufact.lowercased()
//            }
//            if let slug = bullet.slugWeight {
//                slugWeight = slug.lowercased()
//            }
//            let bulletT = bullet.type.lowercased()
//            
//            if bulletN.contains(searchT) || bulletM.contains(searchT) || bulletT.contains(searchT) || bulletW.contains(searchT) || slugWeight.contains(searchT) {
//                filteredBullets.append(bullet)
//            } else { continue }
            
        }
        
        if (filteredBullets.count == bulletsForFiltering.count) || searchT.count == 0 {
            searchActive = false
        } else {
            searchActive = true
        }
        
        tableView.reloadData()
    }
}


extension GameDetailController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 2
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let vw = UIView(frame: CGRect(x:0, y:0, width:tableView.frame.size.width, height:18))
        let lineFrame = CGRect(x: 16, y: 0, width: (tableView.bounds.size.width - 32), height: 1)
        let line = UIView(frame: lineFrame)
        line.backgroundColor = UIColor.orange
        vw.addSubview(line)
        //vw.backgroundColor = UIColor.white
        if #available(iOS 13.0, *) {
            vw.backgroundColor = UIColor.systemBackground
        } else {
            vw.backgroundColor = UIColor.white
        }
        return vw
    }
}

