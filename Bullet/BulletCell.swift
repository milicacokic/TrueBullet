//
//  BulletCell.swift
//  TrueBullet
//
//  Created by Milica Cokic on 4/22/18.
//  Copyright © 2018 Milica Cokic. All rights reserved.
//

import UIKit

final class BulletCell: UITableViewCell, ReusableView {
    
    @IBOutlet private weak var labelName: UILabel!
    @IBOutlet private weak var labelManufact: UILabel!
    @IBOutlet private weak var labelType: UILabel!
    @IBOutlet private weak var labelWeight: UILabel!
    @IBOutlet weak var manufactLabel: NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
        cleanup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        addSeparatorLineToTop()
    }
    
    
    func configure(with cc: Bullet) {
        
        labelManufact.text = cc.name
        labelWeight.text = cc.type
        labelType.text = cc.manufacturer
        if let weight = cc.weight {
            var shortWeight = 1
            if !(weight - floor(weight) > 0.000001) {
                shortWeight = Int(weight)
                labelName.text = "\(String(describing: shortWeight)) gr"
            } else {
                labelName.text = "\(String(describing: weight)) gr"
            }
            
            if labelManufact.text == nil {
                labelManufact.text = cc.name
                if !(weight - floor(weight) > 0.000001) {
                    shortWeight = Int(weight)
                    labelName.text = "\(String(describing: shortWeight)) gr"
                } else {
                    labelName.text = "\(String(describing: weight)) gr"
                }
                //labelName.text = "\(String(describing: weight)) gr"
                labelWeight.text = cc.type
                labelType.text = ""
                //labelManufact.isHidden = true
                //manufactLabel.constant = 0
            }
        }
        
        if let slugWeight = cc.slugWeight {
            
            guard let manufacturer = cc.manufacturer else { return }

            let combinedName = "\(cc.name)  \(manufacturer)"
            labelManufact.text = combinedName
            labelName.text = slugWeight
            labelWeight.text = cc.type
            labelType.text = ""
        }
    }
}



private extension BulletCell {
    func cleanup() {
        labelName.text = nil
        labelManufact.text = nil
        labelType.text = nil
        labelWeight.text = nil
    }
}


