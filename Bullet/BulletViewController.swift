//
//  BulletViewController.swift
//  TrueBullet
//
//  Created by Milica on 11/27/18.
//  Copyright © 2018 Milica Cokic. All rights reserved.
//

import UIKit


final class BulletViewController: UIViewController {
    
    
    @IBOutlet private weak var collectionView: UICollectionView!
    @IBOutlet private weak var tableView: UITableView!
    
    private var myBulletStorage = BulletStorage.shared
    
    lazy var searchBar = UISearchBar(frame: CGRect.zero)
    private var searchActive: Bool = false
    
    private var filteredBullets: [Bullet] = []
    private var filteredByCategory: [Bullet] = []
    
    private var shotguns: [Shotgun] = []
    
    private var cellTitles: [String] = ["Rifle", "Shotgun Slug", "Handgun"]
    private var headerLabel: String = "Rifle"
    
    private var index: Int = 0
    private var isArrayArrived = false
    private var isHandgunArrayArrived = false
    private var isShotgunArrayArrived = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getBulletArray()
        getHandgunArray()
        getShotgunArray()

        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false
       
        tableView.estimatedRowHeight = 44
        tableView.rowHeight = UITableView.automaticDimension
        headerLabel = "Rifle"
        self.tableView.reloadData()
        
        setupKeyboardNotificationHandlers()
        setSearchBar()
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationItem.backBarButtonItem?.tintColor = UIColor(red: 0/255.0, green: 144/255.0, blue: 81/255.0, alpha: 1.0)
        
        settingColors()
        
//        myBulletStorage.readBulletsFromBundleWithRadius()
//        myBulletStorage.readHandgunFromBundleWithRadius()
//        myBulletStorage.readShotgunFromBundleWithRadius()
        
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        //self.tableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let index = self.tableView.indexPathForSelectedRow {
            self.tableView.deselectRow(at: index, animated: true)
        }
    }
    
    private func settingColors() {
        
        if #available(iOS 13.0, *) {
            collectionView.backgroundColor = UIColor.systemBackground
        } else {
            collectionView.backgroundColor = UIColor.white
        }
        
        if #available(iOS 13.0, *) {
            view.backgroundColor = UIColor.systemBackground
        } else {
            view.backgroundColor = UIColor.white
        }
    }
    
    private func setSearchBar() {
        searchBar.delegate = self
        searchBar.returnKeyType = UIReturnKeyType.done
        searchBar.placeholder = "Search"
        navigationItem.titleView = searchBar
    }
    
    private func getBulletArray() {
        myBulletStorage.getArray {
            [weak self] in
            DispatchQueue.main.async {
                //self?.bulletsFromBullets = bullets
                //sad samo treba ispraviti da se ne donosi niz ovde, nego da table view radi direktno sa myBulletStorage.bullets
                self?.isArrayArrived = true
                guard let bullets = self?.myBulletStorage.bullets else { return }
                if self?.index == 0 {
                   self?.filteredByCategory = bullets
                }
                self?.tableView.reloadData()
            }
        }
        if !isArrayArrived {
            isArrayArrived = true
            if index == 0 {
                filteredByCategory = myBulletStorage.bullets
            }
            tableView.reloadData()
        }
    }
    
    
    private func getHandgunArray() {
        myBulletStorage.getHandgunArray {
            [weak self] in
            DispatchQueue.main.async {
                self?.isHandgunArrayArrived = true
                guard let handgunBullets = self?.myBulletStorage.bulletsFromHandgun else { return }
                if self?.index == 2 {
                    self?.filteredByCategory = handgunBullets
                }
                self?.tableView.reloadData()
            }
        }
        if !isHandgunArrayArrived {
            isHandgunArrayArrived = true
            if index == 2 {
                filteredByCategory = myBulletStorage.bulletsFromHandgun
            }
            tableView.reloadData()
        }
    }
    
    
    private func getShotgunArray() {
        myBulletStorage.getShotgunArray {
            [weak self] in
            DispatchQueue.main.async {
                self?.isShotgunArrayArrived = true
                guard let shotgunBullets = self?.myBulletStorage.bulletsFromShotgun else { return }
                if self?.index == 1 {
                    self?.filteredByCategory = shotgunBullets
                }
                self?.tableView.reloadData()
            }
        }
        if !isShotgunArrayArrived {
            isShotgunArrayArrived = true
            if index == 1 {
                filteredByCategory = myBulletStorage.bulletsFromShotgun
            }
            tableView.reloadData()
        }
    }
}


extension BulletViewController {
    
    func setupKeyboardNotificationHandlers() {
        let nc = NotificationCenter.default
        
        nc.addObserver(forName: UIResponder.keyboardWillShowNotification, object: nil, queue: .main, using: keyboardWillShowNotificationHandler)
        
        nc.addObserver(forName: UIResponder.keyboardDidHideNotification, object: nil, queue: .main, using: keyboardDidHideNotificationHandler)
    }
    
    
    @objc func keyboardWillShowNotificationHandler(_ notification: Notification) {
        
        if let userInfo = notification.userInfo {
            if let frameValue = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
                let frame = frameValue.cgRectValue
                let keyboardVisibleHeight = frame.size.height
                
                self.tableView.contentInset.bottom = keyboardVisibleHeight
            }
        }
    }
    
    @objc func keyboardDidHideNotificationHandler(_ notification: Notification) {
        self.tableView.contentInset.bottom = 0
    }
}


extension BulletViewController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: BulletCategoryCell = collectionView.dequeueReusableCell(withReuseIdentifier: "BulletCategoryCell", for: indexPath) as! BulletCategoryCell
        cell.populate(with: UIImage(named: "\(indexPath.row)bullet")!, and: cellTitles[indexPath.row])
        return cell
    }
}


extension BulletViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
//        myBulletStorage.saveBulletsWithRadius()
//        myBulletStorage.saveHandgunWithRadius()
//        myBulletStorage.saveShotgunWithRadius()
        
        searchBar.text = nil
        searchActive = false
        
        if indexPath.row == 0 {
            index = 0
            filteredByCategory = myBulletStorage.bullets
            headerLabel = cellTitles[0]
        }
        else if indexPath.row == 1 {
            index = 1
            filteredByCategory = myBulletStorage.bulletsFromShotgun
            headerLabel = cellTitles[1]
        } else if indexPath.row == 2 {
            index = 2
            filteredByCategory = myBulletStorage.bulletsFromHandgun
            headerLabel = cellTitles[2]
        }
        tableView.reloadData()
    }
}


extension BulletViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        //guard let layout = collectionViewLayout as? UICollectionViewFlowLayout else { fatalError("Must be used with flow layout only") }
        print(self.view.frame.width)
        print(self.collectionView.frame.width)
        print((self.collectionView.frame.width - 20)/3)
        print(floor((self.collectionView.frame.width - 20) / 3))
        return CGSize(width: (self.collectionView.frame.width - 20) / 3, height: self.collectionView.frame.height / 6 * 5)
        //return CGSize(width: self.collectionView.frame.width / 3, height: self.collectionView.frame.height / 6 * 5)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
          return 10
    }
}


extension BulletViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (searchActive) {
            if filteredBullets.count > 0 {
                return filteredBullets.count
            } else {
                return 0
            }
        } else {
            return filteredByCategory.count
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: BulletCell = tableView.dequeueReusableCell(withIdentifier: BulletCell.reuseIdentifier, for: indexPath) as! BulletCell
        
        if searchActive && filteredBullets.count > 0 {
            let sortedFilteredBullets2 = filteredBullets.sorted(by: { $0.name < $1.name })
            let sortedFilteredBullets = sortedFilteredBullets2.sorted(by: { $0.radius < $1.radius })
            let cc = sortedFilteredBullets[indexPath.row]
            cell.setAccessoryView()
            cell.configure(with: cc)
        } else {
            let sortedFilteredBullets2 = filteredByCategory.sorted(by: { $0.name < $1.name })
            let sortedBullets = sortedFilteredBullets2.sorted(by: { $0.radius < $1.radius })
            let cc = sortedBullets[indexPath.row]
            cell.setAccessoryView()
            cell.configure(with: cc)
        }
        return cell
    }
}


extension BulletViewController: UISearchBarDelegate {
    
    //ovo se poziva cim pocnes da kucas u Search polje
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true
        searchBar.returnKeyType = .done
        searchBar.setShowsCancelButton(true, animated: true)
    }

    
    //ovo se poziva samo na klik na Done???
//    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
//        searchActive = false
//        searchBar.resignFirstResponder()
//        searchBar.setShowsCancelButton(false, animated: true)
//    }
    
    //ovo se poziva samo na klik na Cancel
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
        searchBar.resignFirstResponder()
        searchBar.text = nil
        searchBar.setShowsCancelButton(false, animated: true)
        tableView.reloadData()
    }
    
    //ovo se desava na klik na Done
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = true
        searchBar.resignFirstResponder()
        searchBar.setShowsCancelButton(false, animated: true)
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText.count == 0 {
            searchActive = false
            // ako se javi neki bug kod ovog searchovanja, odkomentarisi ove dve donje linije
            //searchBar.resignFirstResponder()
            //searchBar.setShowsCancelButton(false, animated: true)
            filteredBullets = filteredByCategory
        } else {
            searchActive = true
            filteredBullets = []
            let searchT = searchText.lowercased()
            for bullet in filteredByCategory {
                
                var fullBulletID = ""
                var fullBulletIDTrim = ""
                var bulletM = ""
                var bulletMTrim = ""
                var slugWeight = ""
                var bulletW = ""
                var bulletWTrim = ""
                var weightForSearch = ""
                var weightForSearchTrim = ""
            
                if let weight = bullet.weight {
                    bulletW = String(describing: weight).lowercased()
                    bulletWTrim = bulletW.replacingOccurrences(of: " ", with: "")
                    var shortWeight = 1
                    if !(weight - floor(weight) > 0.000001) {
                        shortWeight = Int(weight)
                        weightForSearch = "\(String(describing: shortWeight)) gr"
                        weightForSearchTrim = weightForSearch.replacingOccurrences(of: " ", with: "")
                    } else {
                        weightForSearch = "\(String(describing: weight)) gr"
                        weightForSearchTrim = weightForSearch.replacingOccurrences(of: " ", with: "")
                    }
                }
                
                let bulletN = bullet.name.lowercased()
                let bulletNTrim = bulletN.replacingOccurrences(of: " ", with: "")
                let bulletT = bullet.type.lowercased()
                let bulletTTrim = bulletT.replacingOccurrences(of: " ", with: "")
                
                if let manufact = bullet.manufacturer {
                    bulletM = manufact.lowercased()
                    bulletMTrim = bulletM.replacingOccurrences(of: " ", with: "")
                }
                if let slug = bullet.slugWeight {
                    slugWeight = slug.lowercased()
                    fullBulletID = "\(bulletN) \(bulletM) \(slugWeight) \(bulletT)"
                    fullBulletIDTrim = "\(bulletNTrim) \(bulletMTrim) \(slugWeight) \(bulletTTrim)"
                } else {
                    fullBulletID = "\(bulletN) \(weightForSearch) \(bulletT) \(bulletM)"
                    fullBulletIDTrim = "\(bulletNTrim) \(weightForSearchTrim) \(bulletTTrim) \(bulletMTrim)"
                }
                
                //let searchTNoSpaces = searchT.trimmingCharacters(in: .whitespacesAndNewlines)
                let searchTNoSpaces = searchT.replacingOccurrences(of: " ", with: "")
                print("Trimmed search text: \(searchTNoSpaces)")
                if fullBulletID.contains(searchT) || bulletN.contains(searchT) || bulletM.contains(searchT) || bulletT.contains(searchT) || bulletW.contains(searchT) || slugWeight.contains(searchT) || fullBulletID.contains(searchTNoSpaces) || bulletN.contains(searchTNoSpaces) || bulletM.contains(searchTNoSpaces) || bulletT.contains(searchTNoSpaces) || bulletW.contains(searchTNoSpaces) || slugWeight.contains(searchTNoSpaces) || fullBulletIDTrim.contains(searchT) || bulletNTrim.contains(searchT) || bulletMTrim.contains(searchT) || bulletTTrim.contains(searchT) || bulletWTrim.contains(searchT) || slugWeight.contains(searchT)
                {
                    filteredBullets.append(bullet)
                }
                else { continue }
            }
        }
        tableView.reloadData()
    }
    
}


extension BulletViewController: UITableViewDelegate {
    
    private func showBulletDetails(bullet: Bullet) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let vc = storyboard.instantiateViewController(withIdentifier: "PossibleGamesController") as? PossibleGamesController else {
            fatalError("could not find PossibleGamesController")
        }
        vc.bullet = bullet
        show(vc, sender: self)
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let sortedFilteredBullets2 = filteredByCategory.sorted(by: { $0.name < $1.name })
        let sortedBullets = sortedFilteredBullets2.sorted(by: { $0.radius < $1.radius })
        //let sortedBullets = filteredByCategory.sorted(by: { $0.radius < $1.radius })
        //let sortedBullets = filteredByCategory.sorted(by: { $0.name < $1.name })
        //var selectedBullet = sortedBullets[indexPath.row]
        var selectedBullet = Bullet(name: "", type: "", weight: 0, speed0: 0, chok0: 0, radius: 0)
        
        if searchActive && filteredBullets.count > 0 {
            //let sortedFilteredBullets = filteredBullets.sorted(by: { $0.radius < $1.radius })
            //let sortedFilteredBullets = filteredBullets.sorted(by: { $0.name < $1.name })
            let sortedFilteredBullets2 = filteredBullets.sorted(by: { $0.name < $1.name })
            let sortedFilteredBullets = sortedFilteredBullets2.sorted(by: { $0.radius < $1.radius })
            selectedBullet = sortedFilteredBullets[indexPath.row]
        } else {
            selectedBullet = sortedBullets[indexPath.row]
        }
        showBulletDetails(bullet: selectedBullet)
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let vw = UIView(frame: CGRect(x:0, y:0, width:tableView.frame.size.width, height:18))
        let lineFrame = CGRect(x: 16, y: 12, width: (tableView.bounds.size.width - 32), height: 1)
        let line = UIView(frame: lineFrame)
        line.backgroundColor = UIColor.orange
        vw.addSubview(line)
        
        let label = PaddingLabel(frame: CGRect(x:0, y:0, width:tableView.frame.size.width, height:2*(line.frame.origin.y)))
        label.center = vw.center
        if self.view.traitCollection.horizontalSizeClass == UIUserInterfaceSizeClass.regular {
            label.font = UIFont(name: "Avenir-Heavy", size: 20.0)
        } else {
            label.font = UIFont(name: "Avenir-Heavy", size: 17.0)
        }
        
        if #available(iOS 13.0, *) {
                  label.textColor = UIColor.label
                  label.backgroundColor = UIColor.systemBackground
               } else {
                   label.textColor = UIColor.black
                   label.backgroundColor = UIColor.white
               }
        label.text = headerLabel
        label.frame.origin.y = 0
        label.frame.size.width = label.intrinsicContentSize.width
        label.frame.origin.x = vw.frame.size.width/2 - label.frame.size.width/2
        //label.translatesAutoresizingMaskIntoConstraints = false
        //label.backgroundColor = UIColor.white
        vw.addSubview(label)
        if #available(iOS 13.0, *) {
                           vw.backgroundColor = UIColor.systemBackground
                      } else {
                           vw.backgroundColor = UIColor.white
                      }
        //vw.backgroundColor = UIColor.white
        
        return vw
        
    }
    
}




