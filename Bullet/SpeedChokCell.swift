//
//  SpeedChokCell.swift
//  TrueBullet
//
//  Created by Milica Cokic on 6/12/18.
//  Copyright © 2018 Milica Cokic. All rights reserved.
//

import UIKit

final class SpeedChokCell: UITableViewCell, ReusableView {
    
    @IBOutlet private weak var labelS0: UILabel!
    @IBOutlet private weak var labelS100: UILabel!
    @IBOutlet private weak var labelS200: UILabel!
    @IBOutlet private weak var labelS300: UILabel!
    @IBOutlet private weak var labelC0: UILabel!
    @IBOutlet private weak var labelC100: UILabel!
    @IBOutlet private weak var labelC200: UILabel!
    @IBOutlet private weak var labelC300: UILabel!


    override func awakeFromNib() {
        super.awakeFromNib()
        //cleanup()
    }
    
    
    func configure(with cc: Bullet) {
        labelS0.text = String(describing: cc.speed0)
        labelS100.text = String(describing: cc.speed100)
        labelS200.text = String(describing: cc.speed200)
        labelS300.text = String(describing: cc.speed300)
        labelC0.text = String(describing: cc.chok0)
        labelC0.font = UIFont.boldSystemFont(ofSize: 24.0)
        labelC100.text = String(describing: cc.chok100)
        labelC100.font = UIFont.boldSystemFont(ofSize: 24.0)
        labelC200.text = String(describing: cc.chok200)
        labelC200.font = UIFont.boldSystemFont(ofSize: 24.0)
        labelC300.text = String(describing: cc.chok300)
        labelC300.font = UIFont.boldSystemFont(ofSize: 24.0)
    }
    

}


private extension SpeedChokCell {
    
    func cleanup() {
        labelS0.text = nil
        labelS100.text = nil
        labelS200.text = nil
        labelS300.text = nil
        labelC0.text = nil
        labelC100.text = nil
        labelC200.text = nil
        labelC300.text = nil
    }
}

