//
//  BulletCategoryCell.swift
//  TrueBullet
//
//  Created by Milica on 11/27/18.
//  Copyright © 2018 Milica Cokic. All rights reserved.
//

import UIKit

final class BulletCategoryCell: UICollectionViewCell {
    
    @IBOutlet private weak var cellImage: UIImageView!
    @IBOutlet weak var cellText: UILabel!
    
    
    func populate(with image: UIImage, and text: String) {
        cellImage.image = image
        cellText.text = text
    }
}
