//
//  Shotgun.swift
//  TrueBullet
//
//  Created by Milica on 2/20/19.
//  Copyright © 2019 Milica Cokic. All rights reserved.
//

import Foundation


final class Shotgun: Codable {
    
    enum Orientation: String, Codable {
        case M, F, MF
    }
    
    var name: String
    var type: String
    var orientation: Orientation = .MF
    var slugWeight: String
    var manufacturer: String
    var chok0: Double
    //var shellLength: String
    var radius: Double
    
    
//    init(gauge: String, shellLength: String, slugWeight: String, type: String, chok0: Double, radius: Double) {
//        self.name = gauge
//        self.shellLength = shellLength
//        self.slugWeight = slugWeight
//        self.type = type
//        self.chok0 = chok0
//        self.radius = radius
//    }
    
    init(name: String, manufacturer: String, slugWeight: String, type: String, chok0: Double, radius: Double) {
        self.name = name
        self.manufacturer = manufacturer
        self.slugWeight = slugWeight
        self.type = type
        self.chok0 = chok0
        self.radius = radius
    }
}

