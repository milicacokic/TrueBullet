//
//  GameStorage.swift
//  TrueBullet
//
//  Created by Milica Cokic on 5/1/18.
//  Copyright © 2018 Milica Cokic. All rights reserved.
//

import Foundation


final class GameStorage {
    
    private var finished = false
    
    var specificGames: [GameSpecific] = [] {
        didSet {
            self.finished = true
            guard let callback = uiCallback else {return}
            callback()
        }
    }

    //typealias Callback = ([GameSpecific]) -> Void
    typealias Callback2 = () -> Void
    private var uiCallback: Callback2?
    
    let encoder = JSONEncoder()
    let decoder = JSONDecoder()
    var data: Data?
    
    static let shared = GameStorage()
    
    private init() {
        readGameFromBundle()
    }
}


extension GameStorage {
    
    func getArray(callback: @escaping Callback2) {
        if finished == true {
            callback()
        } else {
            uiCallback = callback
        }
    }
    
    
    func readGameFromBundle() {
        DispatchQueue.global(qos: .background).async {
            guard let fileURL = Bundle.main.path(forResource: "games", ofType: "txt") else {fatalError()}
            let url = URL(fileURLWithPath: fileURL)
            
            var games2: [Game] = []
            guard let data = FileManager.default.contents(atPath: url.path) else { return }
            do {
                games2 = try self.decoder.decode([Game].self, from: data)
            } catch let error {
                print(error)
            }
            let specGame = self.makeSpecificGames(games: games2)
            self.specificGames = specGame
        }
    }
}


extension GameStorage {
    
    func filterByCategory(_ index: Int) -> [GameSpecific] {
        
        var filteredByCategory: [GameSpecific] = []
        
        if index == 0 {
            for game in specificGames {
                guard let predator = game.predator else {return []}
                if let weight = game.weightMF {
                    if weight > 0.5 && weight < 10 && !predator {
                        filteredByCategory.append(game)
                    }
                }
                if let weightSp = game.weightM {
                    if weightSp > 0.5 && weightSp < 10 && !predator {
                        filteredByCategory.append(game)
                    }
                }
            }
            return filteredByCategory
        } else if index == 1 {
            for game in specificGames {
                guard let predator = game.predator else {return []}
                if let weight = game.weightMF {
                    if weight >= 10 && weight < 40 && !predator {
                        filteredByCategory.append(game)
                    }
                }
                if let weightSp = game.weightM {
                    if weightSp >= 10 && weightSp < 40 && !predator {
                        filteredByCategory.append(game)
                    }
                }
            }
            return filteredByCategory
            
        }
         else if index == 2 {
            for game in specificGames {
                guard let predator = game.predator else {return []}
                if let weight = game.weightMF {
                    if weight >= 40 && weight < 110 && !predator {
                        filteredByCategory.append(game)
                    }
                }
                if let weightSp = game.weightM {
                    if weightSp >= 40 && weightSp < 110 && !predator {
                        filteredByCategory.append(game)
                    }
                }
            }
            return filteredByCategory
        }
            else if index == 3 {
                for game in specificGames {
                    guard let predator = game.predator else {return []}
                    if let weight = game.weightMF {
                        if weight >= 110 && weight < 330 && !predator {
                            filteredByCategory.append(game)
                        }
                    }
                    if let weightSp = game.weightM {
                        if weightSp >= 110 && weightSp < 330 && !predator {
                            filteredByCategory.append(game)
                        }
                    }
                }
                return filteredByCategory
                
            } else if index == 4 {
                for game in specificGames {
                    guard let predator = game.predator else {return []}
                    if let weight = game.weightMF {
                        if weight >= 330 && weight < 770 && !predator {
                            filteredByCategory.append(game)
                        }
                    }
                    if let weightSp = game.weightM {
                        if weightSp >= 330 && weightSp < 770 && !predator {
                            filteredByCategory.append(game)
                        }
                    }
  
                }
                return filteredByCategory
                
            } else if index == 5 {
                for game in specificGames {
                    guard let predator = game.predator else {return []}
                    if let weight = game.weightMF {
                        if weight >= 770 && weight < 1540 && !predator {
                            filteredByCategory.append(game)
                        }
                    }
                    if let weightSp = game.weightM {
                        if weightSp >= 770 && weightSp < 1540 && !predator {
                            filteredByCategory.append(game)
                        }
                    }

                }
                return filteredByCategory
                
            } else if index == 6 {
                for game in specificGames {
                    guard let predator = game.predator else {return []}
                    if let weight = game.weightMF {
                        if weight >= 1540 && weight < 3100 && !predator {
                            filteredByCategory.append(game)
                        }
                    }
                    if let weightSp = game.weightM {
                        if weightSp >= 1540 && weightSp < 3100 && !predator {
                            filteredByCategory.append(game)
                        }
                    }
                }
                return filteredByCategory
            }
                else if index == 7 {
                    for game in specificGames {
                        if let weight = game.weightMF {
                            if weight >= 3100 {
                                filteredByCategory.append(game)
                            }
                        }
                        if let weightSp = game.weightM {
                            if weightSp >= 3100 {
                                filteredByCategory.append(game)
                            }
                        }
                    }
                    return filteredByCategory
                } else {
                   for game in specificGames {
                     guard let predator = game.predator else {return []}
                     if predator {
                        filteredByCategory.append(game)
                      }
                    }
                   return filteredByCategory
               }
        }
}


extension GameStorage {
    
    func makeSpecificGames(games: [Game]) -> [GameSpecific] {
        
        var myGames = games
        var specificGames: [GameSpecific] = []
        
        var a = 0
        var b = 1
        
        if games.count > 0 {
        
        for _ in 1..<games.count {
            
            if games[a].tag == games[b].tag {
                let specificGame = GameSpecific()
                specificGame.name = games[a].name
                specificGame.predator = games[a].predator
                if games[a].avWeight > games[b].avWeight {
                    specificGame.weightM = games[a].avWeight
                    specificGame.weightF = games[b].avWeight
                } else {
                    specificGame.weightM = games[b].avWeight
                    specificGame.weightF = games[a].avWeight
                }
                
                specificGames.append(specificGame)
                myGames = myGames.filter { $0.tag != games[a].tag }
            }
            
            a += 1
            b += 1
        }
            
        for game in myGames {
                let specificGame = GameSpecific()
                specificGame.name = game.name
                specificGame.predator = game.predator
                specificGame.weightMF = game.avWeight
                specificGames.append(specificGame)
        }
            
        } else {
            specificGames = []
        }
        
        return specificGames
    }
}
