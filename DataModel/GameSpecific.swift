//
//  GameSpecific.swift
//  TrueBullet
//
//  Created by Milica on 2/16/19.
//  Copyright © 2019 Milica Cokic. All rights reserved.
//

import Foundation


final class GameSpecific {
    
    enum Orientation: String, Codable {
        case M, F, MF
    }
    
    var name: String = "test name"
    var tag: Int?
    //var tagMF: Orientation?
    var tagMF: Orientation = .MF
    var weightM: Double?
    var weightF: Double?
    var weightMF: Double?
    var predator: Bool?
    
    
    func bottomChok(_ weight: Double) -> Double { // bottomChok je minimum na ustima cevi za tu tezinu divljaci
        
        switch weight {
            
        case 0...9:
            return 4
        case 10...25:
            return 30
        case 41...110:
            return 80
        case 111...330:
            return 250
        case 331...770:
            return 550
        case 771...1540:
            return 1100
        case 1541...3100:
            return 2200
        case let x where x > 3100:
            return 3000
        default:
            fatalError( "No game of that size" )
        }
    }
    
    func bottomChok50(_ bottomChok: Double) -> Double {
        
        return (bottomChok + (50 * bottomChok)/100)
    }
    
    func bottomChokDouble50(_ bottomChok: Double) -> Double {
        
        let bottomChokDouble = 2 * bottomChok
        return (bottomChokDouble + (50 * bottomChokDouble)/100)
    }
    
    
    func nextMinimum(_ weight: Double) -> Double {
        
        switch weight {
        
        case 0...9:
            return 30
        case 10...25:
            return 80
        case 25...110:
            return 250
        case 111...330:
            return 550
        case 331...770:
            return 1100
        case 771...1540:
            return 2200
        case 1541...3100:
            return 3000
        case let x where x > 3100:
            return 6000 // promeniti u pravi podatak
        default:
            fatalError( "No game of that size" )
        }
        
    }
    
    func previousMinimum(_ weight: Double) -> Double {
        
        switch weight {
            
        case 0...9:
            return 4 //?
        case 10...25:
            return 4
        case 25...110:
            return 30
        case 111...330:
            return 80
        case 331...770:
            return 250
        case 771...1540:
            return 550
        case 1541...3100:
            return 1100
        case let x where x > 3100:
            return 2200
        default:
            fatalError( "No game of that size" )
        }
        
    }
    
    
    func predatorNextMinimum(_ weight: Double) -> Double {
        
        switch weight {
            
        case 0...9:
            return 80
        case 10...40:
            return 250
        case 25...110:
            return 550
        case 111...330:
            return 1100
        case 331...770:
            return 2200
        case 771...1540:
            return 3000
        case 1541...3100:
            return 6000 //promeniti u pravi podatak
        case let x where x > 3100:
            return 10000 // promeniti u pravi podatak
        default:
            fatalError( "No game of that size" )
        }
        
    }
        
        
}
