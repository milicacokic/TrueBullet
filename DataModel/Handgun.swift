//
//  Handgun.swift
//  TrueBullet
//
//  Created by Milica on 2/20/19.
//  Copyright © 2019 Milica Cokic. All rights reserved.
//

import Foundation

final class Handgun: Codable {
    
    enum Orientation: String, Codable {
        case M, F, MF
    }
    
    var name: String
    var type: String
    var orientation: Orientation = .MF
    var weight: Double
    var speed0: Double
    var chok0: Double
    var radius: Double
   
    
    init(name: String, type: String, weight: Double, speed0: Double, chok0: Double, radius: Double) {
        self.name = name
        self.type = type
        self.weight = weight
        self.speed0 = speed0
        self.chok0 = chok0
        self.radius = radius
    }
}
