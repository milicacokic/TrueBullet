//
//  BulletStorage.swift
//  TrueBullet
//
//  Created by Milica Cokic on 4/22/18.
//  Copyright © 2018 Milica Cokic. All rights reserved.
//

import Foundation


final class BulletStorage {
    
    var bulletsWithRadius: [Bullet] = []
    var handgunWithRadius: [Bullet] = []
    var shotgunWithRadius: [Bullet] = []
    
    private var finishedBullets = false {
        didSet {
            guard let callback = uiCallback2 else { return }
            callback()
        }
    }
    private var finishedHandgun = false {
        didSet {
            guard let callback = uiCallback3 else { return }
            callback()
        }
    }
    private var finishedShotgun = false {
        didSet {
            guard let callback = uiCallback4 else { return }
            callback()
        }
    }
    
    
    private var trueAll: [Int] = [] {
        didSet {
            guard let callback = uiCallback else { return }
            if trueAll.count == 3 {
               bulletsAll = bullets + bulletsFromHandgun + bulletsFromShotgun
               callback(bulletsAll)
            }
        }
    }
    
    typealias Callback = ([Bullet]) -> Void
    typealias Callback2 = () -> Void
    
    private var uiCallback: Callback?
    private var uiCallback2: Callback2?
    private var uiCallback3: Callback2?
    private var uiCallback4: Callback2?
    
    var bullets: [Bullet] = []
    var bulletsFromHandgun: [Bullet] = []
    var bulletsFromShotgun: [Bullet] = []
    
    var bulletsAll: [Bullet] = []
    
    let encoder = JSONEncoder()
    let decoder = JSONDecoder()
    var data: Data?
    
    static let shared = BulletStorage()
    
    private init() {
        readBulletsFromBundle()
        readHandgunFromBundle()
        readShotgunFromBundle()
    }
}


extension BulletStorage {
    
//    func readBulletsFromBundle(callback: @escaping Callback) {
//        DispatchQueue.global(qos: .background).async {
//            guard let fileURL = Bundle.main.path(forResource: "bullets", ofType: "txt") else {fatalError()}
//            let url = URL(fileURLWithPath: fileURL)
//
//            var bullets2: [Bullet] = []
//            guard let data = FileManager.default.contents(atPath: url.path) else { return }
//            do {
//                bullets2 = try self.decoder.decode([Bullet].self, from: data)
//            } catch let error {
//                print(error)
//            }
//            self.bullets = bullets2
//            self.finishedBullets = true
//            self.trueAll.append(1)
//            callback(bullets2)
//        }
//    }
    
    
    func bulletsAll(callback: @escaping ([Bullet]) -> Void) {
        if (finishedBullets && finishedHandgun && finishedShotgun) {
            bulletsAll = bullets + bulletsFromHandgun + bulletsFromShotgun
            callback(bulletsAll)
        } else {
            uiCallback = callback
        }
    }
    
    
    func getArray(callback: @escaping Callback2) {
        if finishedBullets == true {
            callback()
        } else {
            uiCallback2 = callback
        }
    }
    
    
    func getHandgunArray(callback: @escaping Callback2) {
        if finishedHandgun == true {
            callback()
        } else {
            uiCallback3 = callback
        }
    }
    
    func getShotgunArray(callback: @escaping Callback2) {
        if finishedShotgun == true {
            callback()
        } else {
            uiCallback4 = callback
        }
    }
    
    
    func readBulletsFromBundle() {
        DispatchQueue.global(qos: .background).async {
            //guard let fileURL = Bundle.main.path(forResource: "bullets", ofType: "txt") else {fatalError()}
            guard let fileURL = Bundle.main.path(forResource: "radius", ofType: "txt") else {fatalError()}
            let url = URL(fileURLWithPath: fileURL)
            
            var bullets2: [Bullet] = []
            guard let data = FileManager.default.contents(atPath: url.path) else { return }
            do {
                bullets2 = try self.decoder.decode([Bullet].self, from: data)
            } catch let error {
                print(error)
            }
            self.bullets = bullets2
            self.finishedBullets = true
            self.trueAll.append(1)
        }
    }
    
    
    func readHandgunFromBundle() {
        DispatchQueue.global(qos: .background).async {
            //guard let fileURL = Bundle.main.path(forResource: "handgun", ofType: "txt") else {fatalError()}
            guard let fileURL = Bundle.main.path(forResource: "radiusHandgun", ofType: "txt") else {fatalError()}
            let url = URL(fileURLWithPath: fileURL)
            
            var handguns2: [Handgun] = []
            guard let data = FileManager.default.contents(atPath: url.path) else { return }
            do {
                handguns2 = try self.decoder.decode([Handgun].self, from: data)
            } catch let error {
                print(error)
            }
            let bulletsFromHandgun2 = self.createBullet(from: handguns2)
            self.bulletsFromHandgun = bulletsFromHandgun2
            self.finishedHandgun = true
            self.trueAll.append(2)
        }
    }
    
    
    func readShotgunFromBundle() {
        DispatchQueue.global(qos: .background).async {
            //guard let fileURL = Bundle.main.path(forResource: "shotgun", ofType: "txt") else {fatalError()}
            guard let fileURL = Bundle.main.path(forResource: "radiusShotgun", ofType: "txt") else {fatalError()}
            let url = URL(fileURLWithPath: fileURL)
            
            var shotguns2: [Shotgun] = []
            guard let data = FileManager.default.contents(atPath: url.path) else { return }
            do {
                shotguns2 = try self.decoder.decode([Shotgun].self, from: data)
            } catch let error {
                print(error)
            }
            let bulletsFromShotgun2 = self.createBullet(from: shotguns2)
            self.bulletsFromShotgun = bulletsFromShotgun2
            self.finishedShotgun = true
            self.trueAll.append(3)
        }
    }

}



extension BulletStorage {
    
    func createBullet(from: [Handgun]) -> [Bullet] {
        var bullets = [Bullet]()
        for handgun in from {
            
            let bullet = Bullet(name: handgun.name, type: handgun.type, weight: handgun.weight, speed0: handgun.speed0, chok0: handgun.chok0, radius: handgun.radius)
            bullets.append(bullet)
        }
        return bullets
    }
    
    
    func createBullet(from: [Shotgun]) -> [Bullet] {
        var bullets = [Bullet]()
        for shotgun in from {
            
//            let bullet = Bullet(name: shotgun.name, shellLength: shotgun.shellLength, slugWeight: shotgun.slugWeight, type: shotgun.type, chok0: shotgun.chok0)
            let bullet = Bullet(name: shotgun.name, manufacturer: shotgun.manufacturer, slugWeight: shotgun.slugWeight, type: shotgun.type, chok0: shotgun.chok0, radius: shotgun.radius)
            bullets.append(bullet)
        }
        return bullets
    }
    
}


extension BulletStorage {
    func readBulletsFromBundleWithRadius() {
 
            guard let fileURL = Bundle.main.path(forResource: "bullets", ofType: "txt") else {fatalError()}
            let url = URL(fileURLWithPath: fileURL)
            
            var bulletsR: [Bullet] = []
            guard let data = FileManager.default.contents(atPath: url.path) else { return }
            do {
                bulletsR = try self.decoder.decode([Bullet].self, from: data)
            } catch let error {
                print(error)
            }
        
        for bullet in bulletsR {
            //bullet.radius = 0.5
            switch bullet.name {
                        case "17 HMR", "17 Hornet", "17 Remington", "17 WSM":
                            bullet.radius = 0.172
                        case "204 Ruger":
                            bullet.radius = 0.204
                        case "5.45x39":
                            bullet.radius = 0.22
                        case "218 Bee", "22 Hornet", "22 SavageHP", "22 WMR", "22-250 Remington", "220 Swift", "222 Remington", "223 Remington", "223 WSSM", "225 Winchester", "5.6x50 Mag", "5.6x50R Mag", "5.6x57":
                            bullet.radius = 0.224
                        case "240 Weatherby", "243 Winchester", "6 XC", "6 mm Creedmoor", "6 mm Remington":
                            bullet.radius = 0.243
                        case "25-06 Remington", "25-35 Winchester", "250 Savage", "257 Roberts", "257 Weatherby Mag.":
                            bullet.radius = 0.257
                        case "260 Remington", "264 Winchester Mag.", "6.5 Creedmoor", "6.5 Grendel", "6.5 PRC", "6.5-284", "6.5x50 Arisaka", "6.5x52 Carcano", "6.5x54 Mannlicher", "6.5x55 Swedish Mauser", "6.5x57 Mauser", "6.5x57R", "6.5x65 RWS", "6.5x68":
                            bullet.radius = 0.264
                        case "270 WSM", "270 Weatherby Mag.", "270 Winchester", "6.8 Rem.SPC":
                            bullet.radius = 0.277
                        case "275 Rigby", "280 Ackley Improved", "280 Remington", "7 mm Blaser Mag.", "7 mm Mauser", "7 mm Rem.Mag.", "7 mm STW", "7 mm Weatherby Mag.", "7-08 Remington", "7-30 Waters", "7x33 Sako", "7x57R", "7x64 Brenneke", "7x65R Brenneke":
                            bullet.radius = 0.284
                        case "30 Carbine", "30 R Blaser", "30 TC", "30-06 Spr.", "30-30 Winchester", "30-378 Weatherby Mag.", "30-40 Krag", "300 Blackout", "300 Blaser Mag.", "300 H&H Mag", "300 RCM", "300 Rem.UM", "300 Savage", "300 WSM", "300 Weatherby Mag.", "300 Win.Mag.", "307 Winchester", "308 Marlin Express", "308 Norma m.", "308 Winchester", "7.5x55 Swiss":
                            bullet.radius = 0.308
                        case "303 British", "7.62x39 Soviet", "7.7x58 Arisaka":
                            bullet.radius = 0.311
                        case "32-20 Winchester", "7.62x54R Russ":
                            bullet.radius = 0.312
                        case "7.65 Mauser Argent.":
                            bullet.radius = 0.313
                        case "8x57 JR":
                            bullet.radius = 0.318
                        case "32 Win.Spec.":
                            bullet.radius = 0.321
                        case "8.2x53R", "8mm Mauser", "8x57 JRS", "8x57 JS", "8x57 Mauser", "8x60 S", "8x68 S", "8x75 RS":
                            bullet.radius = 0.323
                        case "338 Blaser Mag.", "338 Lapua M", "338 Marlin Express", "338 RCM", "338 Rem.UM", "338 Win.Mag.", "338-06 A Square", "338-378 Weatherby Mag.", "340 Weatherby Mag.":
                            bullet.radius = 0.338
                        case "348 Winchester":
                            bullet.radius = 0.348
                        case "35 Remington", "35 Whelen", "356 Winchester", "358 Norma Mag.", "358 Winchester":
                            bullet.radius = 0.358
                        case "9.3x53R Sako", "9.3x57", "9.3x62", "9.3x64", "9.3x66 Sako", "9.3x74R":
                            bullet.radius = 0.365
                        case "375 Blaser Mag.", "375 H&H", "375 Ruger", "375 Winchester", "376 Steyr", "378 Weatherby Mag.":
                            bullet.radius = 0.375
                        case "9.3x72R":
                            bullet.radius = 0.376
                        case "38-55 Winchester":
                            bullet.radius = 0.379
                        case "450/400 NE 3\"":
                            bullet.radius = 0.407
                        case "405 Winchester":
                            bullet.radius = 0.412
                        case "416 Remington Mag.", "416 Rigby", "416 Ruger", "416 Weatherby Mag.", "500/416 NE":
                            bullet.radius = 0.416
                        case "404 Jeffery", "404 Rimless":
                            bullet.radius = 0.422
                        case "44-40 Winchester", "444 Marlin":
                            bullet.radius = 0.429
                        case "45-70 Govern.", "450 Bushmaster", "450 Marlin", "450 NE 3 1/4\"", "450 Rigby", "458 Lott", "458 Win.Mag.", "460 Weatherby Mag.":
                            bullet.radius = 0.458
                        case "470 NE":
                            bullet.radius = 0.475
                        case "505 Gibbs":
                            bullet.radius = 0.505
                        case "500 Jeffery", "500 NE", "500 NE 3\"":
                            bullet.radius = 0.51
                        case "577 NE":
                            bullet.radius = 0.585
                        case "600 NE":
                            bullet.radius = 0.62
                        case "700 NE":
                            bullet.radius = 0.7
                        default:
                            bullet.radius = 0
                        }
        }
        
        bulletsWithRadius = bulletsR
    }
    
    
    func saveBulletsWithRadius() {
            
            let appSupportFolder = try! FileManager.default.url(for: .applicationSupportDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
            let fileURL = appSupportFolder.appendingPathComponent("radius.txt")
            print("**********")
        
            print("\(fileURL.path)")
            print("************")
            print("")
            
            let newData = try? encoder.encode(bulletsWithRadius)
            //data = newData
            do {
                guard let data = newData else { return }
                try data.write(to: fileURL, options: [])
                
            } catch let error {
                print(error)
            }
    }
    
    func readHandgunFromBundleWithRadius() {
        guard let fileURL = Bundle.main.path(forResource: "handgun", ofType: "txt") else {fatalError()}
        let url = URL(fileURLWithPath: fileURL)
        
        var handgunsR: [Handgun] = []
        guard let data = FileManager.default.contents(atPath: url.path) else { return }
        do {
            handgunsR = try self.decoder.decode([Handgun].self, from: data)
        } catch let error {
            print(error)
        }
        let bulletsFromHandgunR = self.createBullet(from: handgunsR)
       
        for bullet in bulletsFromHandgunR {
            switch bullet.name {
            case "5.45x18 Soviet":
                bullet.radius = 0.22
            case "7.62x25 Tokarev":
                bullet.radius = 0.307
            case "7.63x25 Mauser", "7.65x21 Luger":
                bullet.radius = 0.308
            case "32 ACP":
                bullet.radius = 0.309
            case "32 H&R Magnum", "32 NAA", "32 S&W Long", "32 Smith&Wesson":
                bullet.radius = 0.312
            case "7.5 mm Swiss Revol.":
                bullet.radius = 0.317
            case "8 mm Lebel Revolv.", "8 mm Rast-Gasser":
                bullet.radius = 0.323
            case "8 mm Roth Steyr":
                bullet.radius = 0.329
            case "9 mm Bayard Long", "9 mm Browning Long", "9 mm Luger", "9 mm Steyr", "9 mm Ultra", "9x23 Winchester":
                bullet.radius = 0.355
            case "357 Magnum", "357 SIG", "38 Casull", "38 Smith&Wesson", "38 Special", "38 Super Automatic", "380 ACP":
                bullet.radius = 0.357
            case "9 mm Makarov":
                bullet.radius = 0.363
            case "375/454 Woodswalker":
                bullet.radius = 0.375
            case "10 mm Automatic", "40 Smith&Wesson":
                bullet.radius = 0.4
            case "41 Remington Magnum":
                bullet.radius = 0.41
            case "10.4 Italian Revolver":
                bullet.radius = 0.422
            case "44 Remington Magnum", "44 S&W Russian", "44 S&W Special", "44-40 Winchester", "44/454 Woodswalker":
                bullet.radius = 0.429
            case "45 ACP", "45 Glock Auto Pistol", "45 Smith&Wesson Schofield", "45 Winchester Magnum", "460 Smith&Wesson":
                bullet.radius = 0.452
            case "45 Colt", "454 Casull", "455 Webley MK II":
                bullet.radius = 0.454
            case "480 Ruger":
                bullet.radius = 0.475
            case "50 Action Express", "50 Smith&Wesson":
                bullet.radius = 0.5
            default:
                bullet.radius = 0
            }
        }
        
        handgunWithRadius = bulletsFromHandgunR
    }
    
    func saveHandgunWithRadius() {
        
        let appSupportFolder = try! FileManager.default.url(for: .applicationSupportDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        let fileURL = appSupportFolder.appendingPathComponent("radiusHandgun.txt")
        print("**********")
        
        print("\(fileURL.path)")
        print("************")
        print("")
        
        let newData = try? encoder.encode(handgunWithRadius)
        do {
            guard let data = newData else { return }
            try data.write(to: fileURL, options: [])
            
        } catch let error {
            print(error)
        }
    }
    
    
    func readShotgunFromBundleWithRadius() {
        guard let fileURL = Bundle.main.path(forResource: "shotgun", ofType: "txt") else {fatalError()}
        let url = URL(fileURLWithPath: fileURL)
        
        var shotgunsR: [Shotgun] = []
        guard let data = FileManager.default.contents(atPath: url.path) else { return }
        do {
            shotgunsR = try self.decoder.decode([Shotgun].self, from: data)
        } catch let error {
            print(error)
        }
        let bulletsFromShotgunR = self.createBullet(from: shotgunsR)
        
        for bullet in bulletsFromShotgunR {
            switch bullet.name {
            case "10 G.":
                bullet.radius = 0.775
            case "12 G.":
                bullet.radius = 0.73
            case "16 G.":
                bullet.radius = 0.662
            case "20 G.":
                bullet.radius = 0.615
            case "410 G.":
                bullet.radius = 0.41
            default:
                bullet.radius = 0
            }
        }
        
        shotgunWithRadius = bulletsFromShotgunR
    }
    
    
    func saveShotgunWithRadius() {
        
        let appSupportFolder = try! FileManager.default.url(for: .applicationSupportDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        let fileURL = appSupportFolder.appendingPathComponent("radiusShotgun.txt")
        print("**********")
        
        print("\(fileURL.path)")
        print("************")
        print("")
        for bullet in shotgunWithRadius {
            print(bullet)
        }
        let newData = try? encoder.encode(shotgunWithRadius)
        do {
            guard let data = newData else { return }
            try data.write(to: fileURL, options: [])
            
        } catch let error {
            print(error)
        }
    }
}
