//
//  Game.swift
//  TrueBullet
//
//  Created by Milica Cokic on 4/30/18.
//  Copyright © 2018 Milica Cokic. All rights reserved.
//

import Foundation


final class Game: Codable {
    
    enum Orientation: String, Codable {
        case M, F, MF
    }
    
    var name: String
    var tag: Int? // (0,0), (1,1), ove vrednosti moraju da se unesu u excel
    var predator: Bool
    var avWeight: Double
    
    init(name: String, tag: Int, avWeight: Double, predator: Bool) {
        self.name = name
        self.tag = tag
        self.avWeight = avWeight
        self.predator = predator
    }
    
    var bottomChok: Double { // bottomChok je minimum na ustima cevi za tu tezinu divljaci
        
        switch avWeight {
         
        case 0...9:
            return 4
        case 10...40:
            return 30
        case 41...110:
            return 80
        case 111...330:
            return 250
        case 331...770:
            return 550
        case 771...1540:
            return 1100
        case 1541...3100:
            return 2200
        case let x where x > 3100:
            return 3000
        default:
            fatalError( "No game of that size" )
        }
        
    }
    
    var bottomChok50: Double {
        
        return (bottomChok + (50 * bottomChok)/100)
    }
    
    var bottomChokDouble50: Double {
        
        let bottomChokDouble = 2 * bottomChok
        return (bottomChokDouble + (50 * bottomChokDouble)/100)
    }
    
    
    var nextMinimum: Double {
        
        switch avWeight {
            
        case 0...9:
            return 30
        case 10...40:
            return 80
        case 41...110:
            return 250
        case 111...330:
            return 550
        case 331...770:
            return 1100
        case 771...1540:
            return 2200
        case 1541...3100:
            return 3000
        case let x where x > 3100:
            return 6000 // promeniti u pravi podatak
        default:
            fatalError( "No game of that size" )
        }
        
    }
    
    
    var predatorNextMinimum: Double {
        
        switch avWeight {
            
        case 0...9:
            return 80
        case 10...40:
            return 250
        case 41...110:
            return 550
        case 111...330:
            return 1100
        case 331...770:
            return 2200
        case 771...1540:
            return 3000
        case 1541...3100:
            return 6000 //promeniti u pravi podatak
        case let x where x > 3100:
            return 10000 // promeniti u pravi podatak
        default:
            fatalError( "No game of that size" )
        }
    }
        
}



// Ono što je na nuli (na ustima cevi, 0 ) u granicama  težine te divljači je usual. Šta je duplo je possible. Sve preko duplo je nepotrebno jako. Za opasne predatore je duplo usual, a sve preko toga je possible.
// Kad kažem u granicama težine , to je otprilike najčešći  primerci vrste. Na primer jelen vapiti je oko250 do 350 kila (550 -770 funti). Prosečno oko 660 funti.Za njega je usual 660 chok funti ,a possible do 1320 chok funti.

// Usual na ustima cevi je minimum za divljač te klase
//    Za light game do 110 funti minimum na ustima cevi je 80 chok lbd
//    Za medium game preko 110 do 330 lbs je 250 chok lbs
//    Za large game od 330 do 770 lbd minimum je na ustima cevi je 550 chok lbs
//    Za very large game of 770 do 1540 lbs minimum je 1100 chok lbs
//    Za hevy game od 1540 do 3100 lbs minimum je 2200 chok lbs
//    Za super hevy game tešku preko 3100 lbs je sve što ima preko 3000 chok lbs na ustima cevi

//    Znači ako je divljač po težini u određenoj klasi chok vrednosti za tu klasu od minimalne pa do 50 posto veće su usual, od 50 posto do sledećeg minimuma su possible. Za predatore (agresivne - lav tigar) sve
//    duplo.

//    Znači ako ja average weight 440 lbs to je large game (između 330 i 770 lbs) usual je između 550 i 825 chok lbs .possible je između 825 i 1100  chok lbs.
//    Za agresivne mace je ako je maca od 440 funti onda je usual od 1100 do 1650 vhok lbs a possible je od 1650 do 2199 chok lbs. Za njih dakle ide viša kategorija. Inače primer za macu od 440 funti su vrednosti za svaku drugu divljač od 880 funti.

