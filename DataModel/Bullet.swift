//
//  Bullet.swift
//  TrueBullet
//
//  Created by Milica Cokic on 4/22/18.
//  Copyright © 2018 Milica Cokic. All rights reserved.
//

import Foundation

final class Bullet: Codable {
    
    enum Orientation: String, Codable {
        case M, F, MF
    }
    
    var name: String
    var type: String
    var orientation: Orientation = .MF
    var weight: Double?
    var manufacturer: String?
    var speed0: Double?
    var speed100: Double?
    var speed200: Double?
    var speed300: Double?
    var chok0: Double
    var chok100: Double?
    var chok200: Double?
    var chok300: Double?
    var radius: Double
    //var isFavorite: Bool
    var slugWeight: String?
    
    
    init(name: String, type: String, weight: Double, manufacturer: String, speed0: Double, speed100: Double, speed200: Double, speed300: Double, chok0: Double, chok100: Double, chok200: Double, chok300: Double, radius: Double) {
        self.name = name
        self.type = type
        self.weight = weight
        self.manufacturer = manufacturer
        self.speed0 = speed0
        self.speed100 = speed100
        self.speed200 = speed200
        self.speed300 = speed300
        self.chok0 = chok0
        self.chok100 = chok100
        self.chok200 = chok200
        self.chok300 = chok300
        self.radius = radius
    }
    

    init(name: String, type: String, weight: Double, speed0: Double, chok0: Double, radius: Double) {
        self.name = name
        self.type = type
        self.weight = weight
        self.speed0 = speed0
        self.chok0 = chok0
        self.manufacturer = nil
        self.radius = radius
        
    }
    
//    init(name: String, shellLength: String, slugWeight: String, type: String, chok0: Double) {
//        self.name = name
//        self.manufacturer = shellLength
//        self.slugWeight = slugWeight
//        self.weight = Double(slugWeight)
//        self.type = type
//        self.chok0 = chok0
//    }
    
    init(name: String, manufacturer: String, slugWeight: String, type: String, chok0: Double, radius: Double) {
        self.name = name
        self.manufacturer = manufacturer
        self.slugWeight = slugWeight
        self.weight = Double(slugWeight)
        self.type = type
        self.chok0 = chok0
        self.radius = radius
    }
}


extension Bullet: Equatable {
    static func == (lhs: Bullet, rhs: Bullet) -> Bool {
            return lhs.name == rhs.name && lhs.type == rhs.type && lhs.manufacturer == rhs.manufacturer && lhs.weight == rhs.weight && lhs.slugWeight == rhs.slugWeight
    }
}


extension Bullet: Hashable {

    func hash(into hasher: inout Hasher) {
        hasher.combine(name)
        hasher.combine(type)
        hasher.combine(weight)
        hasher.combine(manufacturer)
    }
}
