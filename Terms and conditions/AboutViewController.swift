//
//  AboutViewController.swift
//  TrueBullet
//
//  Created by Milica on 6/21/19.
//  Copyright © 2019 Milica Cokic. All rights reserved.
//

import UIKit
import SafariServices
import PDFKit

final class AboutViewController: UIViewController {
    
    @IBOutlet private weak var scrollView: UIScrollView!
    @IBOutlet private weak var aboutAppLabel: UILabel!
    @IBOutlet private weak var container: UIView!
    @IBOutlet private weak var privacyButton: UIButton!
    
    let titleText: String = "True Bullet"

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "True Bullet"
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: nil, action: nil)
        navigationItem.backBarButtonItem?.tintColor = UIColor(red: 0/255.0, green: 144/255.0, blue: 81/255.0, alpha: 1.0)
        
        settingColors()
        
        if self.view.traitCollection.horizontalSizeClass == UIUserInterfaceSizeClass.regular {
           self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "Avenir-Heavy", size: 25)!]
        } else {
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "Avenir-Heavy", size: 18)!]
        }
        
        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 2000)
        
    }
    
    @IBAction func policyTapped(_ sender: UIButton) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let vc = storyboard.instantiateViewController(withIdentifier: "PrivacyPolicyVC") as? PrivacyPolicyVC else {
            fatalError("could not find PrivacyPolicyVC")
        }
        show(vc, sender: self)
    }
    
    
    private func settingColors() {
        
        if #available(iOS 13.0, *) {
            container.backgroundColor = UIColor.systemBackground
            view.backgroundColor = UIColor.systemBackground
            privacyButton.titleLabel?.textColor = UIColor(red: 0/255.0, green: 144/255.0, blue: 81/255.0, alpha: 1.0)
//            privacyButton.backgroundColor = UIColor.systemGray
        } else {
            container.backgroundColor = UIColor.white
            view.backgroundColor = UIColor.white
            privacyButton.titleLabel?.textColor = UIColor(red: 0/255.0, green: 144/255.0, blue: 81/255.0, alpha: 1.0)
        }
        
    }

}
