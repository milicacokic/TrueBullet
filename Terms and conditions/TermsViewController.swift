//
//  TermsViewController.swift
//  TrueBullet
//
//  Created by Milica on 1/19/19.
//  Copyright © 2019 Milica Cokic. All rights reserved.
//

import UIKit

final class TermsViewController: UIViewController {
    
    @IBOutlet private weak var terms: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        terms.text = "Prior to using any of the suggested ammunition in this application, please consider whether that ammunition is legal for hunting a specific type of game. Happy hunting and respect the game!"

    }
    
    @IBAction func didTapAcceptButton() {
//        guard let tabBarVC = storyboard.instantiateInitialViewController() else { fatalError() }
//        show(tabBarVC, sender: self)
        dismiss(animated: true, completion: nil)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        AppDelegate.window?.rootViewController = storyboard.instantiateInitialViewController()
        AppDelegate.window?.makeKeyAndVisible()
        
    }


}
