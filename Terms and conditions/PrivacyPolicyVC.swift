//
//  PrivacyPolicyVC.swift
//  TrueBullet
//
//  Created by Milica on 10/8/19.
//  Copyright © 2019 Milica Cokic. All rights reserved.
//

import UIKit
import PDFKit

class PrivacyPolicyVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
     
        let pdfView = PDFView()
        //let pdfView = PDFView(frame: self.view.bounds)
        //pdfView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        pdfView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(pdfView)
        
        pdfView.autoScales = true
                
        pdfView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        pdfView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor).isActive = true
        pdfView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        pdfView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        
        guard let path = Bundle.main.url(forResource: "Privacy Policy", withExtension: "pdf") else { return }

        if let document = PDFDocument(url: path) {
            
            pdfView.document = document
        }

    }
    
}
