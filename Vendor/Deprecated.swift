//
//  Deprecated.swift
//  TrueBullet
//
//  Created by Milica on 4/1/19.
//  Copyright © 2019 Milica Cokic. All rights reserved.
//

import Foundation


//func filterByUsualNew(selectedGame: GameSpecific?) -> [Bullet] {
//
//        for bullet in myBulletStorage.bulletsAll {
//            bullet.orientation = .MF
//        }
//        var usualBullets: [Bullet] = []
//        var usualMFBullets: [Bullet] = []
//        var usualMBullets: [Bullet] = []
//        var usualFBullets: [Bullet] = []
//
//        guard let game = selectedGame else { return [] }
//        guard let predator = game.predator else {fatalError("No predator data")}
//
//        if game.weightMF != nil && game.weightF == nil && game.weightM == nil {
//
//            guard let weight = game.weightMF else { fatalError("Ipak nema weightMF") }
//
//            if predator {
//
//                for bullet in myBulletStorage.bulletsAll {
//
//                    let bottomChok = game.bottomChok(weight)
//
//                    if bullet.chok0 >= (bottomChok * 2) && bullet.chok0 <= game.bottomChokDouble50(bottomChok) {
//                        bullet.orientation = .MF
//                        usualMFBullets.append(bullet)
//                        print(bullet)
//                    }
//                }
//
//            } else {
//                for bullet in myBulletStorage.bulletsAll {
//
//                    let bottomChok = game.bottomChok(weight)
//
//                    if bullet.chok0 >= bottomChok && bullet.chok0 <= game.bottomChok50(bottomChok) {
//                        bullet.orientation = .MF
//                        usualMFBullets.append(bullet)
//                    }
//                }
//            }
//        }
//
//        else if let weightMale = game.weightM, let weightFem = game.weightF {
//
//            if predator {
//
//                for bullet in myBulletStorage.bulletsAll {
//
//                    let bottomChok = game.bottomChok(weightMale)
//
//                    if bullet.chok0 >= (bottomChok * 2) && bullet.chok0 <= game.bottomChokDouble50(bottomChok) {
//                        bullet.orientation = .M
//                        usualMBullets.append(bullet)
//                    }
//                }
//                for bullet in myBulletStorage.bulletsAll {
//
//                    let bottomChok = game.bottomChok(weightFem)
//
//                    if bullet.chok0 >= (bottomChok * 2) && bullet.chok0 <= game.bottomChokDouble50(bottomChok) {
//                        if bullet.orientation == .MF {
//                            bullet.orientation = .F
//                        } else if bullet.orientation == .M {
//                            bullet.orientation = .MF
//                        }
//                        usualFBullets.append(bullet)
//                    }
//                }
//
//            } else {
//                for bullet in myBulletStorage.bulletsAll {
//
//                    let bottomChok = game.bottomChok(weightMale)
//
//                    if bullet.chok0 >= bottomChok && bullet.chok0 <= game.bottomChok50(bottomChok) {
//                        bullet.orientation = .M
//                        usualMBullets.append(bullet)
//                    }
//                }
//                for bullet in myBulletStorage.bulletsAll {
//
//                    let bottomChok = game.bottomChok(weightFem)
//
//                    if bullet.chok0 >= bottomChok && bullet.chok0 <= game.bottomChok50(bottomChok) {
//                        if bullet.orientation == .MF {
//                            bullet.orientation = .F
//                        } else if bullet.orientation == .M {
//                           bullet.orientation = .MF
//                        }
//                        usualFBullets.append(bullet)
//                    }
//                }
//            }
//        }
//
//
//        if usualMFBullets.count != 0 {
//            usualBullets = usualMFBullets
//
//        } else if usualMBullets.count != 0 || usualFBullets.count != 0 {
//
//            let bullets = usualFBullets + usualMBullets
//            //usualBullets = usualFBullets + usualMBullets
//            usualBullets = Array(Set<Bullet>(bullets))
//        }
//
//        return usualBullets
//    }
//




//func filterByAlsoPossibleNew(selectedGame: GameSpecific?) -> [Bullet] {
//    
//    for bullet in allBullets {
//        bullet.orientation = .MF
//    }
//    
//    var possibleBullets: [Bullet] = []
//    var possibleMFBullets: [Bullet] = []
//    var possibleMBullets: [Bullet] = []
//    var possibleFBullets: [Bullet] = []
//    
//    guard let game = selectedGame else { return [] }
//    guard let predator = game.predator else {fatalError("No predator data")}
//    
//    if game.weightMF != nil && game.weightF == nil && game.weightM == nil {
//        
//        guard let weight = game.weightMF else { fatalError("Ipak nema weightMF") }
//        
//        if predator {
//            
//            for bullet in allBullets {
//                
//                let bottomChok = game.bottomChok(weight)
//                
//                if bullet.chok0 > game.bottomChokDouble50(bottomChok) && bullet.chok0 <= game.predatorNextMinimum(weight) {
//                    bullet.orientation = .MF
//                    possibleMFBullets.append(bullet)
//                }
//                
//                // ovo ispod je dodato: u possible za predatore treba da spadaju i oni obicni meci kao za obicnu zivotinju te tezine koja nije predator, oni ce biti possible jer su malo slabiji, ali ipak mogu da se koriste. Oni koji su 50 i jos 50 posto jaci, ostaju u possiblee kao i do sad.
//                if bullet.chok0 >= bottomChok && bullet.chok0 <= game.bottomChok50(bottomChok) {
//                    bullet.orientation = .MF
//                    possibleMFBullets.append(bullet)
//                }
//            }
//            
//        } else {
//            for bullet in allBullets {
//                
//                let bottomChok = game.bottomChok(weight)
//                
//                if bullet.chok0 > game.bottomChok50(bottomChok) && bullet.chok0 <= game.nextMinimum(weight) {
//                    bullet.orientation = .MF
//                    possibleMFBullets.append(bullet)
//                }
//                
//                if bullet.chok0 > game.previousMinimum(weight) && bullet.chok0 <= bottomChok {
//                    bullet.orientation = .MF
//                    possibleMFBullets.append(bullet)
//                }
//            }
//        }
//    }
//        
//    else if let weightMale = game.weightM, let weightFem = game.weightF {
//        
//        if predator {
//            
//            for bullet in allBullets {
//                
//                let bottomChok = game.bottomChok(weightMale)
//                
//                if bullet.chok0 > game.bottomChokDouble50(bottomChok) && bullet.chok0 <= game.predatorNextMinimum(weightMale) {
//                    bullet.orientation = .M
//                    possibleMBullets.append(bullet)
//                }
//                
//                if bullet.chok0 >= bottomChok && bullet.chok0 <= game.bottomChok50(bottomChok) {
//                    bullet.orientation = .M
//                    possibleMBullets.append(bullet)
//                }
//            }
//            
//            for bullet in allBullets {
//                
//                let bottomChok = game.bottomChok(weightFem)
//                
//                if bullet.chok0 > game.bottomChokDouble50(bottomChok) && bullet.chok0 <= game.predatorNextMinimum(weightFem) {
//                    if bullet.orientation == .MF {
//                        bullet.orientation = .F
//                    } else if bullet.orientation == .M {
//                        bullet.orientation = .MF
//                    }
//                    possibleFBullets.append(bullet)
//                }
//                
//                if bullet.chok0 >= bottomChok && bullet.chok0 <= game.bottomChok50(bottomChok) {
//                    if bullet.orientation == .MF {
//                        bullet.orientation = .F
//                    } else if bullet.orientation == .M {
//                        bullet.orientation = .MF
//                    }
//                    possibleFBullets.append(bullet)
//                }
//                
//            }
//            
//        } else {
//            
//            for bullet in allBullets {
//                
//                let bottomChok = game.bottomChok(weightMale)
//                
//                if bullet.chok0 > game.bottomChok50(bottomChok) && bullet.chok0 <= game.nextMinimum(weightMale) {
//                    bullet.orientation = .M
//                    possibleMBullets.append(bullet)
//                }
//                if bullet.chok0 > game.previousMinimum(weightMale) && bullet.chok0 <= bottomChok {
//                    bullet.orientation = .M
//                    possibleMFBullets.append(bullet)
//                }
//            }
//            
//            for bullet in allBullets {
//                let bottomChok = game.bottomChok(weightFem)
//                
//                if bullet.chok0 > game.bottomChok50(bottomChok) && bullet.chok0 <= game.nextMinimum(weightFem) {
//                    if bullet.orientation == .MF {
//                        bullet.orientation = .F
//                    } else if bullet.orientation == .M {
//                        bullet.orientation = .MF
//                    }
//                    possibleFBullets.append(bullet)
//                }
//                if bullet.chok0 > game.previousMinimum(weightFem) && bullet.chok0 <= bottomChok {
//                    if bullet.orientation == .MF {
//                        bullet.orientation = .F
//                    } else if bullet.orientation == .M {
//                        bullet.orientation = .MF
//                    }
//                    possibleMFBullets.append(bullet)
//                }
//            }
//        }
//    }
//    
//    if possibleMFBullets.count != 0 {
//        possibleBullets = possibleMFBullets
//        
//    } else if possibleMBullets.count != 0 || possibleFBullets.count != 0 {
//        let bullets = possibleFBullets + possibleMBullets
//        //usualBullets = usualFBullets + usualMBullets
//        possibleBullets = Array(Set<Bullet>(bullets))
//    }
//    return possibleBullets
//}
//
//}




// kategorizacija divljaci:

// light big game, antilopa, tompsonova gazela, vuk, roe deer do 110 funti je light - minimum za metak je 80 chok lbs na ustima cevi, ispod nikako, tih 80 je u stvari vrednost bottomChok za objekat Game
// medium big game white tail deer, mule deer, raindeer, black bear od 110 do 330 funti - minimum apsolutni  je 250 chok lbs na ustima cevi, a do 330
// large big game elk, red deer, sambar deer 330 do 770 lbs minimum za metak je 600 lbs do
// very large big game moose, brown bear 770 do 1540 lbs minimum je 1100 na ustima cevi
// heavy game cape buffalo, bizon 1540do 3100 lbs minimum je 2200
// super heavy game ellephant, hipo, rinocerus preko 3100 lbs - minimum na ustima cevi 3000 chok lbs






//        if segmentedControl.selectedSegmentIndex == 0 {
//            if searchActive && animalsFiltered.count > 0 {
//                let sortedFilteredBullets = animalsFiltered.sorted(by: { $0.name < $1.name })
//                let cc = sortedFilteredBullets[indexPath.row]
//                if cc.tagMF == .MF {
//                    cell.setLetterMFtoAccessoryView()
//                } else if cc.tagMF == .M {
//                    cell.setLetterMtoAccessoryView()
//                } else if cc.tagMF == .F {
//                    cell.setLetterFtoAccessoryView()
//                }
//                cell.configure(with: cc)
//            } else {
//                let filteredUsual = usualAnimalsForBullet().sorted(by: { $0.name < $1.name })
//                let cc = filteredUsual[indexPath.row]
//                if cc.tagMF == .MF {
//                    cell.setLetterMFtoAccessoryView()
//                } else if cc.tagMF == .M {
//                    cell.setLetterMtoAccessoryView()
//                } else if cc.tagMF == .F {
//                    cell.setLetterFtoAccessoryView()
//                }
//                cell.configure(with: cc)
//            }
//        } else {
//            if searchActive && animalsFiltered.count > 0 {
//                let sortedFilteredBullets = animalsFiltered.sorted(by: { $0.name < $1.name })
//                let cc = sortedFilteredBullets[indexPath.row]
//                if cc.tagMF == .MF {
//                    cell.setLetterMFtoAccessoryView()
//                } else if cc.tagMF == .M {
//                    cell.setLetterMtoAccessoryView()
//                } else if cc.tagMF == .F {
//                    cell.setLetterFtoAccessoryView()
//                }
//                cell.configure(with: cc)
//            } else {
//                let filteredAlsoPossible = possibleAnimalsForBullet().sorted(by: { $0.name < $1.name })
//                let cc = filteredAlsoPossible[indexPath.row]
//                if cc.tagMF == .MF {
//                    cell.setLetterMFtoAccessoryView()
//                } else if cc.tagMF == .M {
//                    cell.setLetterMtoAccessoryView()
//                } else if cc.tagMF == .F {
//                    cell.setLetterFtoAccessoryView()
//                }
//                cell.configure(with: cc)
//            }
//        }



//        if segmentedControl.selectedSegmentIndex == 0 {
//            if (searchActive) {
//                return animalsFiltered.count
//            } else {
//                return allAnimalsForBullet.count
//                //return usualAnimalsForBullet().count
//            }
//
//        } else {
//            if (searchActive) {
//                return animalsFiltered.count
//            } else {
//                return allAnimalsForBullet.count
//                //return possibleAnimalsForBullet().count
//            }
//       }


//extension GameDetailController {

//    private func showBulletDetails(bullet: Bullet) {
//
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        guard let vc = storyboard.instantiateViewController(withIdentifier: "BulletDetailController") as? BulletDetailController else {
//            fatalError("could not find BulletDetailController")
//        }
//        vc.bullet = bullet
//        show(vc, sender: self)
//    }

//}


//        if segmentedControl.selectedSegmentIndex == 0 {
//            if searchActive && filteredBullets.count > 0 {
//                let sortedFilteredBullets = filteredBullets.sorted(by: { $0.name < $1.name })
//                let cc = sortedFilteredBullets[indexPath.row]
//                if cc.orientation == .MF {
//                    cell.setLetterMFtoAccessoryView()
//                } else if cc.orientation == .M {
//                    cell.setLetterMtoAccessoryView()
//                } else if cc.orientation == .F {
//                    cell.setLetterFtoAccessoryView()
//                }
//                cell.configure(with: cc)
//            } else {
//                let filteredUsual = filterByUsual(selectedGame: game).sorted(by: { $0.name < $1.name })
//                let cc = filteredUsual[indexPath.row]
//                if cc.orientation == .MF {
//                    cell.setLetterMFtoAccessoryView()
//                } else if cc.orientation == .M {
//                    cell.setLetterMtoAccessoryView()
//                } else if cc.orientation == .F {
//                    cell.setLetterFtoAccessoryView()
//                }
//                cell.configure(with: cc)
//            }
//        } else {
//            if searchActive && filteredBullets.count > 0 {
//                let sortedFilteredBullets = filteredBullets.sorted(by: { $0.name < $1.name })
//                let cc = sortedFilteredBullets[indexPath.row]
//                if cc.orientation == .MF {
//                    cell.setLetterMFtoAccessoryView()
//                } else if cc.orientation == .M {
//                    cell.setLetterMtoAccessoryView()
//                } else if cc.orientation == .F {
//                    cell.setLetterFtoAccessoryView()
//                }
//                cell.configure(with: cc)
//            } else {
//                let filteredAlsoPossible = filterByPossible(selectedGame: game).sorted(by: { $0.name < $1.name })
//                let cc = filteredAlsoPossible[indexPath.row]
//                if cc.orientation == .MF {
//                    cell.setLetterMFtoAccessoryView()
//                } else if cc.orientation == .M {
//                    cell.setLetterMtoAccessoryView()
//                } else if cc.orientation == .F {
//                    cell.setLetterFtoAccessoryView()
//                }
//                cell.configure(with: cc)
//            }
//        }
