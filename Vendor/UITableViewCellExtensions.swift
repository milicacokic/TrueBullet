//
//  UITableViewCellExtensions.swift
//  TrueBullet
//
//  Created by Milica on 1/27/19.
//  Copyright © 2019 Milica Cokic. All rights reserved.
//

import Foundation
import UIKit


extension UITableViewCell {
    
    func addSeparatorLineToTop() {
        
        let color = UIColor.orange.cgColor
        
        let shapeLayer: CAShapeLayer = CAShapeLayer()
        let frameSize = frame.size
        let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width, height: 0)
        
        shapeLayer.bounds = shapeRect
        shapeLayer.position = CGPoint(x: frameSize.width/2, y: frameSize.height)
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = color
        shapeLayer.lineWidth = 2.0
        shapeLayer.lineJoin = CAShapeLayerLineJoin.round
        shapeLayer.lineDashPattern = [0,6]
        shapeLayer.path = UIBezierPath(roundedRect: CGRect(x: 16, y: shapeRect.height, width: shapeRect.width - 32, height: 20), cornerRadius: 0).cgPath
        
        layer.addSublayer(shapeLayer)
    }
    
    func setAccessoryView() {
        
        let image = UIImage(named: "final_green_2")
        accessoryView = UIImageView(image: image)
    }
    
    func setBlankAccessoryView() {
        
        let image = UIImage(named: "Image")
        accessoryView = UIImageView(image: image)
    }
    
    func setLetterMtoAccessoryView() {
        
        let image = UIImage(named: "male")
        accessoryView = UIImageView(image: image)
    }
    
    func setLetterFtoAccessoryView() {
        
        let image = UIImage(named: "Female only")
        accessoryView = UIImageView(image: image)
    }
    
    func setLetterMFtoAccessoryView() {
        
        let image = UIImage(named: "gender")
        accessoryView = UIImageView(image: image)
    }
}

