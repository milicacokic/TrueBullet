//
//  UIVCExtensions.swift
//  TrueBullet
//
//  Created by Milica on 1/23/19.
//  Copyright © 2019 Milica Cokic. All rights reserved.
//

import Foundation
import UIKit


extension UIViewController {
   
    func hideKeyboardWhenTappedAround() {
    
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
        self.navigationController?.navigationBar.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
        //resignFirstResponder()
    }
}

