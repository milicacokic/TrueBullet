//
//  ReusableView.swift
//  TrueBullet
//
//  Created by Milica Cokic on 7/9/18.
//  Copyright © 2018 Milica Cokic. All rights reserved.
//

import UIKit

protocol ReusableView {
    
    static var reuseIdentifier: String { get }
}


extension ReusableView where Self: UIView {
    
    static var reuseIdentifier: String {
        return String(describing: self)
    }
}
